import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '', pathMatch: 'full' },
  // {
  //   path: '',
  //   loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  // },
  { path: 'landing', loadChildren: '../pages/landing/landing.module#LandingPageModule' },
  { path: 'forgot-password', loadChildren: '../pages/forgot-password/forgot-password.module#ForgotPasswordPageModule' },
  { path: 'location', loadChildren: '../pages/location/location.module#LocationPageModule' },
  { path: 'home', loadChildren: '../pages/home/home.module#HomePageModule' },
  { path: 'venue', loadChildren: '../pages/venue/venue.module#VenuePageModule' },
  { path: 'profile', loadChildren: '../pages/profile/profile.module#ProfilePageModule' },
  { path: 'walkthrough', loadChildren: '../pages/walkthrough/walkthrough.module#WalkthroughPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
