import { Component } from '@angular/core';

import { Platform, ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppService } from 'src/services/app-service';
import { CacheService } from 'src/services/cache-service';
import { IdentityService } from 'src/services/identity-service';
import { LocationService } from 'src/services/location-service';
import { Constants } from 'src/contracts/constants';
import { LandingPage } from 'src/pages/landing/landing.page';
import { Router } from '@angular/router';
import { WalkthroughPage } from 'src/pages/walkthrough/walkthrough.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  rootPage: any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private appService: AppService,
    private cacheService: CacheService,
    private identityService: IdentityService,
    private locationService: LocationService,
    private modalCtrl: ModalController,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();

      this.hideSplashScreen();
    //   this.splashScreen.hide();

      // this.appService.lockScreenOrientation();

      // this.appService.statusBarStyling();

      this.appService.initializeCache();

      // this.initializeNavEvents();

      // this.routeDeepLink();

      // this.registerBackButton();

        this.router.navigate(['/landing']);
    });
  }

  // routeDeepLink() {
  //     if(this.platform.is('cordova'))
  //     {
  //         this.deeplinks.route({
  //             '/venue': SpotPage,
  //         }).subscribe((match) => {
  //             this.rootPage = LoginPage;
  //             let modal = this.modalController.create(match.$route, { venueId: match.$args.venueId, deeplink: true });
  //             modal.present();
  //             localStorage.setItem("openViaDeepLink", "true");
  //             console.log('Successfully matched route', match);
  //         }, (noMatch) => {
  //             console.log('Got a deeplink that didn\'t match', noMatch);
  //         });
  //     }
  // }

  // registerBackButton() {
  //     this.platform.registerBackButtonAction(() => {
  //         let activePortal = this.app._appRoot._loadingPortal.getActive() ||
  //             this.app._appRoot._modalPortal.getActive() ||
  //             this.app._appRoot._toastPortal.getActive() ||
  //             this.app._appRoot._overlayPortal.getActive();

  //         let view = this.nav.getActive();
  //         let currentRootPage = view.component;
  //         if (currentRootPage != LandingPage && currentRootPage != TabsPage && currentRootPage != LocationPage) {
  //             if (activePortal) {
  //                 activePortal.dismiss();
  //             } else if (this.nav.canGoBack() || view && view.isOverlay) {
  //                 this.nav.pop({ animate: false });
  //             }
  //         }
  //         else {
  //             if (!activePortal) {
  //                 if (!this.hardBackPress) {
  //                     this.hardBackPress = true;
  //                     this.toastService.presentStandardToast('Press again to exit');
  //                 }
  //                 else {
  //                     this.hardBackPress = false;
  //                     if (localStorage.getItem("openViaDeepLink") == "true") {
  //                         localStorage.setItem("openViaDeepLink", "false");
  //                         this.platform.exitApp();
  //                     } else {
  //                         this.appMinimize.minimize();
  //                     }

  //                 }
  //             } else {
  //                 activePortal.dismiss();
  //             }
  //         }
  //     });
  // }

  // initializeNavEvents() {
  //     this.events.subscribe('RouteBroadcastRoot', (page) => {
  //         if (page == "TabsPage") {
  //             this.nav.setRoot(TabsPage);
  //         } else if (page == "LocationModal") {
  //             this.nav.setRoot(LocationPage);
  //         } else if (page == "LandingPage") {
  //             this.nav.setRoot(LandingPage);
  //         } else if (page == "LoginPage") {
  //             this.nav.setRoot(LoginPage);
  //         }
  //         else if (page == Constants.SET_ROOT_LANDING_PAGE_MODAL) {
  //             this.rootPage = InitialisePage;
  //             this.openLandingPage();
  //         }
  //     });

  //     this.events.subscribe('RouteBroadcastPush', (page) => {
  //         if (page == "facebookRegistration") {
  //             this.nav.push(FacebookRegistrationPage);
  //         } else if (page == "SpotPage") {
  //             this.nav.push(SpotPage);
  //         } else if (page == "FriendsPage") {
  //             this.nav.push("FriendsPage");
  //         }
  //     });
  // }
  
  hideSplashScreen()
  {
      if(this.platform.is('cordova'))
      {
          if(this.splashScreen)
          {
              this.splashScreen.hide();
          }
      }
  }
}
