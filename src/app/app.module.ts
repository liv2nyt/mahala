import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, LoadingController, ToastController, AlertController, NavParams } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { Http, HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import { AuthInterceptor } from 'src/providers/AuthInterceptor';
import { IdentityService } from 'src/services/identity-service';
import { ToastService } from 'src/services/toast-service';
import { AppService } from 'src/services/app-service';
import { CacheService } from 'src/services/cache-service';
import { NotificationService } from 'src/services/notification-service';
import { ApiService } from 'src/services/api-service';
import { LocationService } from 'src/services/location-service';
import { ImageService } from 'src/services/image-service';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { LandingPage } from 'src/pages/landing/landing.page';
import { LandingPageModule } from 'src/pages/landing/landing.module';
import { LoginPageModule } from 'src/pages/login/login.module';
import { LocationPageModule } from 'src/pages/location/location.module';
import { HomePageModule } from 'src/pages/home/home.module';
import { ForgotPasswordPageModule } from 'src/pages/forgot-password/forgot-password.module';
import { VenuePageModule } from 'src/pages/venue/venue.module';
import { CallNumber } from "@ionic-native/call-number/ngx";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
import { LaunchNavigator } from "@ionic-native/launch-navigator/ngx";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { RegistrationPageModule } from 'src/pages/registration/registration.module';
import { WalkthroughPageModule } from 'src/pages/walkthrough/walkthrough.module';
import { QrCodePageModule } from 'src/pages/claim-reward/claim-reward.module';
import { RecommendationsPageModule } from 'src/pages/recommendations/recommendations.module';
import { AddRecommendationPageModule } from 'src/pages/recommendations/add-recommendation/add-recommendation.module';
import { SearchVenuePageModule } from 'src/pages/search/search-venue/search-venue.module';
import { ProfilePageModule } from 'src/pages/profile/profile.module';
import { VerifyBillPageModule } from 'src/pages/verify-bill/verify-bill.module';
import { PushNotificationService } from 'src/services/push-notification-service';
import { EventService } from 'src/services/event-service';
import { Firebase } from "@ionic-native/firebase/ngx";
import { Geofence } from "@ionic-native/geofence/ngx";
import { BackgroundGeofenceService } from 'src/services/background-geofence-service';
import { BadgeEarnedPageModule } from 'src/pages/trophy-room/badge-earned/badge-earned.module';
import { NewLevelPageModule } from 'src/pages/trophy-room/new-level/new-level.module';
import { PointsEarnedPageModule } from 'src/pages/trophy-room/points-earned/points-earned.module';
import { DirectMessagePageModule } from 'src/pages/trophy-room/direct-message/direct-message.module';
import { RewardClaimedPageModule } from 'src/pages/trophy-room/reward-claimed/reward-claimed.module';
import { FacebookRegistrationPageModule } from 'src/pages/facebook-registration/facebook-registration.module';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [
    LandingPage
  ],
  imports: [
    AddRecommendationPageModule,
    AppRoutingModule,
    BadgeEarnedPageModule,
    BrowserModule, 
    DirectMessagePageModule,
    HomePageModule,
    HttpModule,
    FacebookRegistrationPageModule,
    ForgotPasswordPageModule,
    IonicModule.forRoot(), 
    LandingPageModule,
    LocationPageModule,
    LoginPageModule,
    NewLevelPageModule,
    PointsEarnedPageModule,
    ProfilePageModule,
    QrCodePageModule,
    RecommendationsPageModule,
    RegistrationPageModule,
    RewardClaimedPageModule,
    SearchVenuePageModule,
    VenuePageModule,
    VerifyBillPageModule,
    WalkthroughPageModule
  ],
  providers: [
    AlertController,
    ApiService,
    AppService,
    BackgroundGeofenceService,
    CacheService,
    CallNumber,
    Camera,
    Crop,
    Diagnostic,
    EventService,
    Facebook,
    FileTransfer,
    Firebase,
    Geofence,
    Geolocation,
    IdentityService,
    InAppBrowser,
    ImageService,
    LaunchNavigator,
    LoadingController,
    LocationAccuracy,
    LocationService,
    NavParams,
    NotificationService,
    PushNotificationService,
    SocialSharing,
    SplashScreen,
    StatusBar,
    ToastController,
    ToastService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: Http, useFactory: authFunction, deps: [XHRBackend, RequestOptions] },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

export function authFunction(backend: XHRBackend, defaultOptions: RequestOptions) {
  return new AuthInterceptor(backend, defaultOptions);
}
