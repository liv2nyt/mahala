import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class EventService{
  private formRefreshAnnouncedSource = new Subject();
  formRefreshSource$ = this.formRefreshAnnouncedSource.asObservable();

  private rewardClaimedAnnouncedSource = new Subject();
  rewardClaimedSource$ = this.rewardClaimedAnnouncedSource.asObservable();

  private billVerifiedAndPointsEarnedAnnouncedSource = new Subject();
  billVerifiedAndPointsEarnedSource$ = this.billVerifiedAndPointsEarnedAnnouncedSource.asObservable();

  publishFormRefresh(){
    this.formRefreshAnnouncedSource.next()
  }

  publishRewardClaimed(){
    this.rewardClaimedAnnouncedSource.next(1);
  }

  publishBillVerifiedAndPointsEarned(pointsEarned){
    this.billVerifiedAndPointsEarnedAnnouncedSource.next(pointsEarned)
  }
}