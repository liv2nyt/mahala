import { Injectable } from "@angular/core";
import { LoadingController, ModalController } from "@ionic/angular";
import { ApiService } from "../services/api-service";
import {
  UserNameLogin,
  ServiceResult,
  LoginResult,
  FacebookLogin
} from "../contracts/contracts";
import { NotificationService } from "./notification-service";
import { CacheService } from "./cache-service";
import { AppService } from "./app-service";
import { LocationService } from './location-service';
import { Router } from '@angular/router';
import { WalkthroughPage } from 'src/pages/walkthrough/walkthrough.page';

@Injectable()
export class IdentityService {
  distanceRadius = 100;

  constructor(
    // private events: Events,
    private loadingCtrl: LoadingController,
    private notificationService: NotificationService,
    private userProvider: ApiService,
    private cacheService: CacheService,
    private appService: AppService,
    private locationService: LocationService,
    private router: Router,
    private modalCtrl: ModalController
  ) {
    var radius = localStorage.getItem("DistanceRadius");
    if (radius != undefined && radius != null) {
      this.distanceRadius = parseInt(radius);
    }
  }

  async initializeUser() {
    var user = this.cacheService.getCachedUser();

    if (user != null || user != undefined) {
      await this.userProvider
        .isAuthenticated()
        .then((response: any) => {
          if (response.WasSuccessful) {
            this.appService.initialize();
            this.validateLocationSettings();
          } else {
            this.authenticateLogin();
          }
        })
        .catch((error) => {
          console.log('Authentication Error', error);
        });
    }
}

authenticateLogin() {
    this.authenticateLoginV2(null)
        .then(() => {
            this.validateLocationSettings();
        })
        .catch(() => {
            // this.openLandingPage();
        });
}

validateLocationSettings() {
    this.locationService.validateLocationSettings()
        .then(() => {
            this.router.navigate(['/home']);
        })
        .catch(() => {
            this.router.navigateByUrl('/location');
        });
}

async openWalkthroughPage() {
      let walkthroughPageModal = await this.modalCtrl.create({
        component: WalkthroughPage
      });
      walkthroughPageModal.onDidDismiss().then(() => {
          this.validateLocationSettings();
      });
      return await walkthroughPageModal.present();
  }

  async authenticateLoginV2(userNameLogin: UserNameLogin) {
    let loading = await this.loadingCtrl.create({
      message: "Logging in ..."
    });

    loading.present();

    var user = this.cacheService.getCachedUser();
    var loginRequest: UserNameLogin = {
      Username: null,
      Password: null
    };
    if (userNameLogin != null || userNameLogin != undefined) {
      loginRequest = userNameLogin;
    } else {
      loginRequest.Username = user.Username;
      loginRequest.Password = user.Password;
    }

    return new Promise((resolve, reject) => {
      this.userProvider
        .authenticateUserWithUserName(loginRequest)
        .then((response: ServiceResult<LoginResult>) => {
          if (response.WasSuccessful) {
            this.appService.initialize();
            // localStorage.setItem("user", JSON.stringify(response.Data));
            // this.cacheService.setCachedUser(response.Data);
            if(userNameLogin != null || userNameLogin != undefined)
            {
              var userWithPassword: LoginResult = response.Data;
              userWithPassword.Password = userNameLogin.Password;
              this.cacheService.setCachedUser(userWithPassword);
            }
            
            localStorage.setItem("userAuthToken", response.Data.Token);
            
            resolve();
            return loading.dismiss();
          } else {
            // loading.dismiss();
            reject();
            return loading.dismiss();
          }
        })
        .catch(() => {
          // loading.dismiss();
          reject();
          return loading.dismiss();
        });
    });
  }

  async registerUserV2(registrationDetails) {
    return new Promise(async (resolve, reject) => {
    let loading = await this.loadingCtrl.create({
      // content: 'Registering...',
      message: "Registering..."
    });

    await loading.present();
    this.userProvider
      .registerUser(registrationDetails)
      .then((response: any) => {
        if (response.WasSuccessful) {
          loading.dismiss();
          var loginDetails: UserNameLogin = {
            Username: registrationDetails.username,
            Password: registrationDetails.hashedPassword
          };
          this.authenticateLoginV2(loginDetails).then(() => {
			// resolve();
			resolve();
          });
        } else {
          loading.dismiss();
          this.notificationService.showErrorMessage(response);
		//   reject();
		reject();
        }
      })
      .catch(err => {
        loading.dismiss();
        if (err.status == 0) {
          this.notificationService.presentNoInternetAlert();
        } else {
          this.notificationService.showErrorMessage(err);
        }
		// reject();
		reject();
      });
    });
  }

  registerExternalUserV2(registrationDetails) {
    return new Promise(async (resolve, reject) => {
    let loading = await this.loadingCtrl.create({
      // content: 'Registering New User ...'
      message: "Registering New User ..."
	});
	
    await loading.present();

    this.userProvider
      .registerExternalUser(registrationDetails)
      .then((response: any) => {
        if (response.WasSuccessful) {
          loading.dismiss();
          var user = this.cacheService.getCachedUser();
          var loginDetails = {
            EmailAddress: user.EmailAddress,
            FacebookId: user.FacebookId
          };

          this.loginExternalV2(loginDetails)
            .then(() => {
              // resolve();
              resolve();
            })
            .catch(() => {
              // reject();
              reject();
            });
        } else {
          loading.dismiss();
          this.notificationService.showErrorMessage(response);
          // reject();
          reject();
        }
      })
      .catch(err => {
        loading.dismiss();
        if (err.status == 0) {
          this.notificationService.presentNoInternetAlert();
        } else {
          this.notificationService.showErrorMessage(err);
        }
        // reject();
        reject();
      });
    });
  }

  loginExternalV2(loginExternalDetails) {
    return new Promise(async (resolve, reject) => {
    let loading = await this.loadingCtrl.create({
      // content: 'Authenticating via Facebook...'
      message: "Authenticating via Facebook..."
    });

    await loading.present();

    var request: FacebookLogin = {
      EmailAddress: loginExternalDetails.EmailAddress,
      FacebookId: loginExternalDetails.FacebookId
    };

    this.userProvider
      .SignInExternalUser(request)
      .then((response: ServiceResult<LoginResult>) => {
        if (response.WasSuccessful) {
          this.appService.initialize();
          localStorage.setItem("user", JSON.stringify(response.Data));
          localStorage.setItem("userAuthToken", response.Data.Token);
          loading.dismiss();
          // resolve(false);
          resolve(false);
        } else {
          // resolve(true);
          resolve(true);
        }
      })
      .catch(err => {
        loading.dismiss();
        console.log("Error connecting to Facebook: ", err);
        if (err.status == 0) {
          this.notificationService.presentNoInternetAlert();
          // reject();
          reject();
        } else if ((err.status = 400)) {
          // resolve(true);
          resolve(true);
        } else {
          this.notificationService.showErrorMessage(err);
          // reject();
          reject();
        }
      });
    });
  }
}
