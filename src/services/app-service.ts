import { Injectable } from "@angular/core";
import { ServiceResult, AppVersionResult } from "../contracts/contracts";
import { ApiService } from "../services/api-service";
import { BackgroundGeofenceService } from "./background-geofence-service";
import { PushNotificationService } from "./push-notification-service";
// import { AppVersion } from "@ionic-native/app-version";
// import { AlertController } from "ionic-angular";
// import { Platform } from "ionic-angular/platform/platform";
import { StatusBar } from "@ionic-native/status-bar";
// import { ScreenOrientation } from "@ionic-native/screen-orientation";
import { Constants } from "../contracts/constants";

@Injectable()
export class AppService {
    constructor(
        private userProvider: ApiService,
        private backgroundGeofenceService: BackgroundGeofenceService,
        private pushNotificationService: PushNotificationService,
        // private appVersion: AppVersion,
        // private alertCtrl: AlertController,
        // private platform: Platform,
        // private screenOrientation: ScreenOrientation,
        // private statusBar: StatusBar
    ) {}

    initialize() {
        this.initializeCache();
        this.initializeGeofenceService();
        this.initializePushNotificationService();
    }

    initializeCache() {
        this.userProvider.initializePushNotificationTypes();

        this.userProvider.initializeVenueTypes();

        this.userProvider.initializeLevels();

        this.userProvider.initializeLeaderBoardTypes();

        this.userProvider.initializeBadgeTypes();
    }

    initializeGeofenceService() {
        this.backgroundGeofenceService.initializeGeofence();
    }

    initializePushNotificationService() {
        var firebaseTokenSaved = localStorage.getItem("FirebaseTokenSaved");
        if (Constants.DEBUG) {
            console.log("Checking Firebase");
        }

        if (firebaseTokenSaved != undefined || firebaseTokenSaved != null) {
            if (firebaseTokenSaved != "true") {
                if (Constants.DEBUG) {
                    console.log("Firebase Initialized")!;
                }
                this.pushNotificationService.initializeFirebase();
            } else {
                this.pushNotificationService.subscribeToPushNotifications();
            }
        } else {
            this.pushNotificationService.initializeFirebase();
        }
    }

    // async getVersionNumber() {
    //     const versionNumber = await this.appVersion.getVersionNumber();
    //     localStorage.setItem("version", versionNumber);
    //     this.checkForUpdate(versionNumber);
    //     console.log(versionNumber);
    // }

    // checkForUpdate(versionNumber) {
    //     var req = {
    //         Version: versionNumber
    //     };
    //     this.userProvider
    //         .checkForUpdate(req)
    //         .then((response: ServiceResult<AppVersionResult>) => {
    //             if (!response.WasSuccessful) {
    //                 this.displayAppUpdateAlert(response.Data);
    //             }
    //         })
    //         .catch(err => {
    //             console.log(err);
    //         });
    // }

    // displayAppUpdateAlert(response: AppVersionResult) {
    //     let alert = this.alertCtrl.create({
    //         title: "Update App",
    //         subTitle: "A new update for our app is available that is required",
    //         buttons: [
    //             {
    //                 text: "Close",
    //                 handler: () => {
    //                     this.platform.exitApp();
    //                 }
    //             },
    //             {
    //                 text: "Update",
    //                 handler: () => {
    //                     if (this.platform.is("ios")) {
    //                         window.open(
    //                             response.IOSLink,
    //                             "_system",
    //                             "location=yes"
    //                         );
    //                     } else {
    //                         window.open(
    //                             response.AndroidLink,
    //                             "_system",
    //                             "location=yes"
    //                         );
    //                     }
    //                     this.platform.exitApp();
    //                 }
    //             }
    //         ]
    //     });
    //     alert.present();
    // }

    // lockScreenOrientation() {
    //     if (this.platform.is("cordova")) {
    //         this.screenOrientation
    //             .lock(this.screenOrientation.ORIENTATIONS.PORTRAIT)
    //             .catch(error => {
    //                 console.log(error.toString());
    //             });
    //     }
    // }

    // statusBarStyling() {
    //     if (this.platform.is("cordova")) {
    //         this.statusBar.styleDefault();
    //         this.statusBar.styleLightContent();
    //         this.statusBar.backgroundColorByHexString("#0E3451");
    //     }
    // }
}
