import { Injectable } from "@angular/core";
import { AlertController } from "@ionic/angular";

@Injectable()

export class NotificationService {

    constructor(
        private alertCtrl: AlertController) {
    }

    async presentAlert(message) {
        const confirm = await this.alertCtrl.create({
            // title: message,
            header: message,
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {

                    }
                }
            ]
        });
        return await confirm.present();
    }

    async presentAlertWithDescription(message: string, description: string) {
        const confirm = await this.alertCtrl.create({
            // title: message,
            header: message,
            message: description,
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {

                    }
                }
            ]
        });
        return await confirm.present();
    }

    async presentNoInternetAlert()
    {
        var alert = await this.alertCtrl.create({
            // title: "Internet connection lost",
            header: "Internet connection lost",
            message: "Please reconnect to the internet and try again",
            buttons: ['OK']
        });
        return await alert.present();
    }

    showErrorMessage(message) {
        if (message.Heading != undefined) {
            this.presentAlertWithDescription(message.Heading, message.Description);
        } else if (message._body != undefined && this.IsJsonString(message._body)) {
            message = JSON.parse(message._body);
            this.presentAlertWithDescription(message.Heading, message.Description);
        } else {
            console.log(message.toString());
            this.presentAlertWithDescription("An error occurred", "An error occurred, please ensure you have a internet connection and try again");
        }
    }

    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
}