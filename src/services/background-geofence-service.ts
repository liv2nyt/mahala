import { Injectable } from "@angular/core";
import { Platform } from "@ionic/angular";
import { ApiService } from "../services/api-service";
import { Geofence } from "@ionic-native/geofence/ngx";
import { NotificationService } from "./notification-service";
import { CacheService } from "./cache-service";

@Injectable()
export class BackgroundGeofenceService {
    venueDistanceData = [];
    OUTER_RADIUS = 150;
    INNER_RADIUS = 50;

    constructor(
        private platform: Platform,
        private userProvider: ApiService,
        private notificationService: NotificationService,
        private geofence: Geofence,
        private cacheService: CacheService
    ) {}

    initializeGeofence() {
        this.platform.ready().then(() => {
            if (this.platform.is("cordova")) {
                this.geofence.initialize().then(
                    () => {
                        console.log("Geofence Plugin Ready");
                        var locationPreference = this.cacheService.getCachedLocationPreference();
                        if (locationPreference != "GPS") {
                            console.log(
                                "Location permissions not given, unable to start geo fences"
                            );
                        } else {
                            this.getVenueDistanceData();
                        }
                    },
                    err => console.log(err)
                );
            }
        });
    }

    getVenueDistanceData() {
        this.userProvider
            .getVenueDistanceData()
            .then((response: any) => {
                if (response.WasSuccessful) {
                    this.venueDistanceData = response.Data;
                    if (this.venueDistanceData.length != 0) {
                        this.setGeofences();
                    }
                } else {
                    this.notificationService.showErrorMessage(response);
                }
            })
            .catch(err => {
                if (err.status == 0) {
                    this.notificationService.presentNoInternetAlert();
                }
            });
    }

    setGeofences() {
        this.venueDistanceData.forEach(item => {
            let fence = {
                id: item.Id.toString(),
                latitude: item.Latitude,
                longitude: item.Longitude,
                radius: this.OUTER_RADIUS,
                transitionType: 1,
                notification: {
                    id: item.Id,
                    title: item.Name,
                    text: item.PushMessage,
                    openAppOnClick: true
                }
            };

            this.geofence.addOrUpdate(fence).then(
                () => {
                    console.log(
                        item.Name + "'s enter geofence added successfully"
                    );
                },
                err =>
                    console.log(
                        "Failed to add or update the fence.",
                        err.toString()
                    )
            );

            fence.id = (item.Id + 1).toString();
            fence.notification.id = fence.notification.id + 1;
            fence.notification.title = "Thanks for visiting " + item.Name;
            fence.notification.text = "Hope to see you soon!";
            fence.transitionType = 2;

            this.geofence.addOrUpdate(fence).then(
                () => {
                    console.log(
                        item.Name + "'s exit geofence added successfully"
                    );
                },
                err =>
                    console.log(
                        "Failed to add or update the fence.",
                        err.toString()
                    )
            );
        });
    }
}
