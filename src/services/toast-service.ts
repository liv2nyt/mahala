import { Injectable } from "@angular/core";
import { ToastController } from "@ionic/angular";

@Injectable()

export class ToastService {
    notification: any = { title: ' ', message: ' ', success: true };

    constructor(private toastCtrl: ToastController) {

    }

    async presentToast(message, status) {
        var cssClass = status ? 'custom-toast-success' : 'custom-toast-danger';

        let toast = await this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top',
            cssClass: cssClass
        });

        return await toast.present();
    }

    presentStandardToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top'
        });
        // toast.present();
    }
}