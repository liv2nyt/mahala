import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
import {
  ServiceResult,
  VenueType,
  PushNotificationType,
  UserNameLogin,
  CardType,
  Level,
  LeaderBoardType,
  BadgeType,
  PaymentType
} from "../contracts/contracts";
import { CacheService } from "./cache-service";
import { Constants } from "../contracts/constants";

@Injectable()
export class ApiService {
  url: string;
  headers: any;

  constructor(public http: Http, private cacheService: CacheService) {
    this.headers = new Headers();
    this.headers.append("Content-Type", "application/json; charset=utf-8");
    this.url = Constants.ENVIRONMENT;
  }

  authenticateUserWithUserName(loginUserDetails: UserNameLogin) {
    var json = JSON.stringify(loginUserDetails);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/Login/Username", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  isAuthenticated() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/Login/IsAuthenticated/")
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  getAddressPredictions(searchTerm) {
    var headerss = new Headers();
    headerss.append("responseType", "arraybuffer");

    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/Distance/AddressPredictions/" + searchTerm)
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  loadProfile(userId) {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/User/" + userId)
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  getMyProfile() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/User")
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  getLatLong(address) {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/Distance/GetCoordinates/" + address)
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  registerUser(registerUserDetails) {
    var json = JSON.stringify(registerUserDetails);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/User/Register", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  resetPasswordNotification(email) {
    var json = JSON.stringify(email);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/Login/ResetPasswordNotification", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  validateOTP(otp) {
    var json = JSON.stringify(otp);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/Login/ValidateOTP", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  resetPassword(password) {
    var json = JSON.stringify(password);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/Login/ResetPassword", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  updateUserCall(profileDetails) {
    var json = JSON.stringify(profileDetails);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .put(this.url + "/User", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  updateVenueCall(profileDetails) {
    var json = JSON.stringify(profileDetails);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .put(this.url + "/Venue/Update", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  registerExternalUser(registerUserDetails) {
    var json = JSON.stringify(registerUserDetails);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/User/Register/Facebook", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  updateVenueTimes(updateTimeData) {
    var json = JSON.stringify(updateTimeData);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .put(this.url + "/Venue/Update/Hours", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  removeTimeSlot(updateTimeData) {
    var json = JSON.stringify(updateTimeData);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .put(this.url + "/Venue/Update/Hours/Remove", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  SignInExternalUser(loginUserDetails) {
    var jsonDataForm = JSON.stringify(loginUserDetails);

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/Login/Facebook", jsonDataForm, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  getVenues(venueDistance) {
    var jsonDataForm = JSON.stringify(venueDistance);

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/Venue/AllVenuesDistance/Filter", jsonDataForm, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  loadSpotCall(spotDistance) {
    var jsonDataForm = JSON.stringify(spotDistance);

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/Venue/VenueDistance", jsonDataForm, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  checkIn(checkInRequest) {
    var jsonDataForm = JSON.stringify(checkInRequest);

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/Venue/CheckIn", jsonDataForm, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  getVenueTypes() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/VenueType")
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  initializeVenueTypes() {
    var venueTypes = this.cacheService.getCachedVenueTypes();
    if (venueTypes != null || venueTypes != undefined) {
      return venueTypes;
    } else {
      this.getVenueTypes()
        .then((response: ServiceResult<VenueType[]>) => {
          if (response.WasSuccessful) {
            this.cacheService.setCachedVenueTypes(response.Data);
            return response.Data;
          }
        })
        .catch((error: ServiceResult<string>) => {});
    }
  }

  initializeLevels() {
    var levels = this.cacheService.getCachedLevels();
    if (levels != null || levels != undefined) {
      return levels;
    } else {
      this.getAllLevels()
        .then((response: ServiceResult<Level[]>) => {
          if (response.WasSuccessful) {
            this.cacheService.setCacheLevels(response.Data);
            return response.Data;
          }
        })
        .catch((error: ServiceResult<string>) => {});
    }
  }

  getPushNotificationTypes() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/App/PushNotificationTypes")
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  initializePushNotificationTypes() {
    var pushNotificationTypes = this.cacheService.getCachedPushNotificationTypes();
    if (pushNotificationTypes != undefined || pushNotificationTypes != null) {
      return pushNotificationTypes;
    } else {
      this.getPushNotificationTypes()
        .then((response: ServiceResult<PushNotificationType[]>) => {
          if (response.WasSuccessful) {
            this.cacheService.setCachedPushNotificationTypes(response.Data);
            return response.Data;
          }
        })
        .catch((error: ServiceResult<string>) => {});
    }
  }

  IsUsernameAvailable(username) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/User/IsUsernameAvailable/" + username, null, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  searchVenues(searchTerm, pageNumber) {
    var headerss = new Headers();
    headerss.append("responseType", "arraybuffer");
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/Venue/Search/" + searchTerm + "/" + pageNumber)
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  checkForUpdate(data) {
    var jsonDataForm = JSON.stringify(data);

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/App/Update", jsonDataForm, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  LinkFirebaseToken(req) {
    var jsonDataForm = JSON.stringify(req);

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/User/Firebase", jsonDataForm, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  loadVenuesVisited(page) {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/User/VenuesVisited/" + page, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  changePassword(data) {
    var jsonDataForm = JSON.stringify(data);

    return new Promise((resolve, reject) => {
      this.http
        .put(this.url + "/User/ChangePassword", jsonDataForm, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  getVenueRecommendations(venueId, page) {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/Recommendation/Venue/" + venueId + "/" + page, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  addRecommendation(recommendation) {
    var json = JSON.stringify(recommendation);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/Recommendation", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  addRecommendationReply(recommendation) {
    var json = JSON.stringify(recommendation);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/Recommendation/Reply", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  postAppFeedback(feedback) {
    var json = JSON.stringify(feedback);
    var params = json;

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/App/Feedback", params, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  getFeedbackSurvey() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/App/FeedbackSurvey", {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <any>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  getUserNotification() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/User/Notifications")
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  getTrophyRoom(venueId) {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/TrophyRoom/" + venueId)
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  getAllLevels() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/Level")
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  getVenueDistanceData() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/App/VenueDistanceData")
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  getLeaderBoardTypes() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/TrophyRoom/LeaderBoardTypes")
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          (data: ServiceResult<LeaderBoardType[]>) => {
            this.cacheService.setCacheLeaderBoardTypes(data.Data);
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  initializeLeaderBoardTypes() {
    var leaderBoardTypes = this.cacheService.getCachedLeaderBoardTypes();
    if (leaderBoardTypes != undefined || leaderBoardTypes != null) {
      return leaderBoardTypes;
    } else {
      this.getLeaderBoardTypes()
        .then((response: ServiceResult<LeaderBoardType[]>) => {
          this.cacheService.setCacheLeaderBoardTypes(response.Data);
          return response.Data;
        })
        .catch((error: ServiceResult<string>) => {});
    }
  }

  getBadgeTypes() {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.url + "/BadgeType")
        .pipe(
          map((response: Response) => {
            var res = response.json();
            return res;
          })
        )
        .subscribe(
          (data: ServiceResult<BadgeType[]>) => {
            this.cacheService.setCacheBadgesTypes(data.Data);
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }

  initializeBadgeTypes() {
    var badgeTypes = this.cacheService.getCachedBadgeTypes();
    if (badgeTypes != null || badgeTypes != undefined) {
      return badgeTypes;
    } else {
      this.getBadgeTypes()
        .then((response: ServiceResult<BadgeType[]>) => {
          this.cacheService.setCacheBadgesTypes(response.Data);
          return response.Data;
        })
        .catch((error: ServiceResult<string>) => {});
    }
  }

  claimReward(req) {
    var jsonDataForm = JSON.stringify(req);

    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/UserReward/Claim", jsonDataForm, {
          headers: this.headers
        })
        .pipe(
          map((response: Response) => {
            var result = <ServiceResult<any>>response.json();
            return result;
          })
        )
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            if (error._body != undefined) {
              error = JSON.parse(error._body);
              resolve(error);
            }
            reject(error);
          }
        );
    });
  }

  checkOut() {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.url + "/Venue/CancelCheckIn", null, {
          headers: this.headers
        })
        .pipe(map((response: Response) => {
          var result = <any>response.json();
          return result;
        }))
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            reject(error);
          }
        );
    });
  }
}
