import { Injectable } from "@angular/core";
import { LoginResult, CardType, Level, LeaderBoardType, BadgeType, VenueType, PushNotificationType, TableRequestResult, PaymentType } from "../contracts/contracts";

@Injectable()

export class CacheService {

    private readonly BADGE_TYPES = "BADGE_TYPES";
    private readonly CARD_TYPES = "CARD_TYPES";
    private readonly LEADER_BOARD_TYPES = "LEADER_BOARD_TYPES";
    private readonly LEVELS = "LEVELS";
    private readonly LOCATION_PREFERENCE = "LOCATION_PREFERENCE";
    private readonly PAYMENT_TYPES = "PAYMENT_TYPES";
    private readonly PUSH_NOTIFICATION_TYPES = "PUSH_NOTIFICATION_TYPES";
    private readonly VENUE_TYPES = "VENUE_TYPES";
    private readonly TABLE_REQUESTS = "TABLE_REQUESTS";

    constructor(
    ) {
    }

    getCachedUser(): LoginResult {
        var json = localStorage.getItem('USER');
        let jsonObj: any = JSON.parse(json);
        let loginRequest: LoginResult = <LoginResult>jsonObj;
        return loginRequest;
    }

    setCachedUser(loginRequest: LoginResult) {
        return new Promise((resolve, reject) => {
            if (loginRequest) {
                localStorage.setItem('USER', JSON.stringify(loginRequest));
                resolve(loginRequest);
            } else {
                console.log("Failed to cache USER");
                reject();
            }
        })
    }

    getBearerToken(): string {
        var json = localStorage.getItem('USER');
        let jsonObj: any = JSON.parse(json);
        let loginRequest: LoginResult = <LoginResult>jsonObj;
        return loginRequest.Token;
    }

    getUserId(): number {
        var json = localStorage.getItem('USER');
        let jsonObj: any = JSON.parse(json);
        let loginRequest: LoginResult = <LoginResult>jsonObj;
        return loginRequest.UserId;
    }

    getUserFacebookId(): string {
        var json = localStorage.getItem('USER');
        let jsonObj: any = JSON.parse(json);
        let loginRequest: LoginResult = <LoginResult>jsonObj;
        return loginRequest.FacebookId;
    }

    getCachedVenueTypes(): VenueType[] {
        var json = localStorage.getItem(this.VENUE_TYPES);
        let jsonObj: any = JSON.parse(json);
        let venueTypes: VenueType[] = <VenueType[]>jsonObj;
        return venueTypes;
    }

    setCachedVenueTypes(venueTypes: VenueType[]) {
        return new Promise((resolve, reject) => {
            if (venueTypes) {
                localStorage.setItem(this.VENUE_TYPES, JSON.stringify(venueTypes));
                resolve(venueTypes);
            } else {
                console.log("Failed to Cache: " + this.VENUE_TYPES);
                reject();
            }
        })
    }

    getCachedPushNotificationTypes(): PushNotificationType[] {
        var json = localStorage.getItem(this.PUSH_NOTIFICATION_TYPES);
        let jsonObj: any = JSON.parse(json);
        let pushNotificationTypes: PushNotificationType[] = <PushNotificationType[]>jsonObj;
        return pushNotificationTypes;
    }

    setCachedPushNotificationTypes(pushNotificationTypes: PushNotificationType[]) {
        return new Promise((resolve, reject) => {
            if (pushNotificationTypes) {
                localStorage.setItem(this.PUSH_NOTIFICATION_TYPES, JSON.stringify(pushNotificationTypes));
                resolve(pushNotificationTypes);
            } else {
                console.log("Failed to Cache: " + this.PUSH_NOTIFICATION_TYPES);
                reject();
            }
        })
    }

    getCachedCardTypes(): CardType[] {
        var json = localStorage.getItem(this.CARD_TYPES);
        let jsonObj: any = JSON.parse(json);
        let cardTypes: CardType[] = <CardType[]>jsonObj;
        return cardTypes;
    }

    setCachedCardTypes(cardTypes: CardType[]) {
        return new Promise((resolve, reject) => {
            if (cardTypes) {
                localStorage.setItem(this.CARD_TYPES, JSON.stringify(cardTypes));
                resolve(cardTypes);
            } else {
                console.log("Failed to Cache: " + this.CARD_TYPES);
                reject();
            }
        })
    }

    getCachedPaymentTypes(): PaymentType[] {
        var json = localStorage.getItem(this.PAYMENT_TYPES);
        let jsonObj: any = JSON.parse(json);
        let paymentTypes: PaymentType[] = <PaymentType[]>jsonObj;
        return paymentTypes;
    }

    setCachedPaymentTypes(paymentTypes: PaymentType[]) {
        return new Promise((resolve, reject) => {
            if (paymentTypes) {
                localStorage.setItem(this.PAYMENT_TYPES, JSON.stringify(paymentTypes));
                resolve(paymentTypes);
            } else {
                console.log("Failed to Cache: " + this.PAYMENT_TYPES);
                reject();
            }
        })
    }

    getCachedLevels(): Level[] {
        var json = localStorage.getItem(this.LEVELS);
        let jsonObj: any = JSON.parse(json);
        let levels: Level[] = <Level[]>jsonObj;
        return levels;
    }

    setCacheLevels(levels: Level[]) {
        return new Promise((resolve, reject) => {
            if (levels) {
                localStorage.setItem(this.LEVELS, JSON.stringify(levels));
                resolve(levels);
            } else {
                console.log("Failed to Cache: " + this.LEVELS);
                reject();
            }
        });
    }

    getCachedLeaderBoardTypes(): LeaderBoardType[] {
        var json = localStorage.getItem(this.LEADER_BOARD_TYPES);
        let jsonObj: any = JSON.parse(json);
        let levels: Level[] = <Level[]>jsonObj;
        return levels;
    }

    setCacheLeaderBoardTypes(leaderBoardTypes: LeaderBoardType[]) {
        return new Promise((resolve, reject) => {
            if (leaderBoardTypes) {
                localStorage.setItem(this.LEADER_BOARD_TYPES, JSON.stringify(leaderBoardTypes));
                resolve(leaderBoardTypes);
            } else {
                console.log("Failed to Cache: " + this.LEADER_BOARD_TYPES);
                reject();
            }
        })
    }

    getCachedBadgeTypes(): BadgeType[] {
        var json = localStorage.getItem(this.BADGE_TYPES);
        let jsonObj: any = JSON.parse(json);
        let badgeTypes: BadgeType[] = <BadgeType[]>jsonObj;
        return badgeTypes;
    }

    setCacheBadgesTypes(badgeTypes: BadgeType[]) {
        return new Promise((resolve, reject) => {
            if (badgeTypes) {
                localStorage.setItem(this.BADGE_TYPES, JSON.stringify(badgeTypes));
                resolve(badgeTypes);
            } else {
                console.log("Failed to Cache: " + this.BADGE_TYPES);
                reject();
            }
        })
    }

    getCachedLocationPreference(): string {
        var preference = localStorage.getItem(this.LOCATION_PREFERENCE);
        return preference;
    }

    setCacheLocationPreference(preference: string) {
        return new Promise((resolve, reject) => {
            if (preference) {
                localStorage.setItem(this.LOCATION_PREFERENCE, preference);
                resolve(preference);
            } else {
                console.log("Failed to Cache: " + this.LOCATION_PREFERENCE);
                reject();
            }
        })
    }

    getCachedTableRequests(): TableRequestResult[] {
        var json = localStorage.getItem(this.TABLE_REQUESTS);
        let jsonObj: any = JSON.parse(json);
        let tableRequests: TableRequestResult[] = <TableRequestResult[]>jsonObj;
        return tableRequests;
    }

    setCacheTableRequests(tableRequests: TableRequestResult[]) {
        return new Promise((resolve, reject) => {
            if (tableRequests) {
                localStorage.setItem(this.TABLE_REQUESTS, JSON.stringify(tableRequests));
                resolve(tableRequests);
            } else {
                console.log("Failed to Cache: " + this.TABLE_REQUESTS);
                reject();
            }
        })
    }

    removeCacheTableRequests() {
        localStorage.removeItem(this.TABLE_REQUESTS);
    }
}