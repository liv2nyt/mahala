import { Injectable } from "@angular/core";
import { Platform, ModalController } from "@ionic/angular";
import { Firebase } from "@ionic-native/firebase/ngx";
import { ApiService } from "../services/api-service";
import {
    NewUserBadgePushResult,
} from "../contracts/contracts";
import { ToastService } from "./toast-service";
import { EventService } from './event-service';
import { PointsEarnedPage } from 'src/pages/trophy-room/points-earned/points-earned.page';
import { NewLevelPage } from 'src/pages/trophy-room/new-level/new-level.page';
import { DirectMessagePage } from 'src/pages/trophy-room/direct-message/direct-message.page';
import { RewardClaimedPage } from 'src/pages/trophy-room/reward-claimed/reward-claimed.page';

@Injectable()
export class PushNotificationService {
    constructor(
        private firebase: Firebase,
        private userProvider: ApiService,
        private platform: Platform,
        private modalCtrl: ModalController,
        private toastService: ToastService,
        private eventService: EventService
    ) {}

    initializeFirebase() {
        if (this.platform.is("cordova")) {
            this.firebase.subscribe("all");
            this.platform.is("android")
                ? this.initializeFirebaseAndroid()
                : this.initializeFirebaseIOS();
        }
    }

    initializeFirebaseAndroid() {
        console.log("Initializing Firebase Android");
        this.firebase
            .getToken()
            .then(token => {
                console.log("Initialized Firebase Android Success");
                this.linkFirebaseToken(token);
            })
            .catch(error => {
                console.log("Initialized Firebase Android Failure", error);
            });
        this.subscribeToPushNotifications();
    }

    initializeFirebaseIOS() {
        console.log("Initializing Firebase iOS");
        this.firebase
            .grantPermission()
            .then(() => {
                this.firebase.getToken().then(token => {
                    console.log("Initialized Firebase iOS Success");
                    this.linkFirebaseToken(token);
                });
                this.subscribeToPushNotifications();
            })
            .catch(error => {
                console.log("Initialized Firebase iOS Failure", error);
                this.firebase.logError(error);
            });
    }

    linkFirebaseToken(token) {
        console.log("Linking Firebase Token");
        var req = {
            FirebaseToken: token
        };
        this.userProvider
            .LinkFirebaseToken(req)
            .then(() => {
                console.log("Linking Firebase Success");
                localStorage.setItem("FirebaseTokenSaved", "true");
            })
            .catch(err => {
                console.log("Linking Firebase Failure", err);
            });
    }

    handleTokenRefresh() {
        console.log("Handling Token Refresh");
        this.firebase.onTokenRefresh().subscribe(
            token => {
                console.log(`The new token is ${token}`);
                this.linkFirebaseToken(token);
            },
            error => {
                console.log("Error refreshing token", error);
            }
        );
    }

    subscribeToPushNotifications() {
        this.platform.ready().then(() => {
            if (this.platform.is("cordova")) {
                this.handleTokenRefresh();
                console.log("Subscribing to Firebase Push Notifications");
                this.firebase.onNotificationOpen().subscribe(
                    response => {
                        console.log(
                            "Subscribed to Firebase Push Notifications"
                        );
                        !response.tap
                            ? console.log(
                                  "The user was using the app when the notification arrived..."
                              )
                            : console.log(
                                  "The app was closed when the notification arrived..."
                              );

                        console.log(response);

                        var body = this.platform.is("android")
                            ? response.body
                            // : response.aps.alert.body;
                            : response.notification.body;
                        var isGreen = true;

                        switch (+response.type) {
                            case 1:
                                if (
                                    localStorage.getItem(
                                        "VenueRecommendation"
                                    ) != undefined
                                ) {
                                    var venueRecommendation = localStorage.getItem(
                                        "VenueRecommendation"
                                    );
                                    localStorage.setItem(
                                        "VenueRecommendation",
                                        (
                                            Number(venueRecommendation) + 1
                                        ).toString()
                                    );
                                } else {
                                    localStorage.setItem(
                                        "VenueRecommendation",
                                        "1"
                                    );
                                }
                                localStorage.setItem(
                                    "VenueRecommendationId",
                                    response.entityId
                                );
                                // this.events.publish("updateTabBadge");
                                localStorage.setItem(
                                    "rootTo",
                                    "VenueRecommendation"
                                );
                                break;
                            case 2:
                                if (
                                    localStorage.getItem(
                                        "VenueRecommendationReply"
                                    ) != undefined
                                ) {
                                    var venueRecommendationReply = localStorage.getItem(
                                        "VenueRecommendationReply"
                                    );
                                    localStorage.setItem(
                                        "VenueRecommendationReply",
                                        (
                                            Number(venueRecommendationReply) + 1
                                        ).toString()
                                    );
                                } else {
                                    localStorage.setItem(
                                        "VenueRecommendationReply",
                                        "1"
                                    );
                                }
                                localStorage.setItem(
                                    "VenueRecommendationReplyId",
                                    response.entityId
                                );
                                // this.events.publish("updateTabBadge");
                                localStorage.setItem(
                                    "rootTo",
                                    "VenueRecommendationReply"
                                );
                                break;
                            case 3:
                                console.log("Reward Claimed");
                                // this.events.publish("rewardClaimed");
                                this.eventService.publishRewardClaimed();
                                this.openRewardClaimedModal();
                                break;
                            case 4:
                                console.log("Badge Earned");
                                var newUserBadgePushResult: NewUserBadgePushResult = JSON.parse(
                                    response.newUserBadgePushResult
                                );
                                if (newUserBadgePushResult != undefined) {
                                    if (
                                        newUserBadgePushResult
                                            .NewUserBadgeResult.BadgeTypeId == 2
                                    ) {
                                        this.openBadgeEarnedModal(
                                            newUserBadgePushResult
                                        );
                                    } else {
                                        this.openBadgeEarnedModal(
                                            newUserBadgePushResult
                                        );
                                    }
                                }
                                break;
                            case 5:
                                console.log("New Reward Available");
                                // this.openMessageModal(response.venueName, response.body);
                                break;
                            case 6:
                                console.log("Direct Message from Restaurant");
                                this.openMessageModal(response.venueName, response.body);
                                break;
                            case 7:
                                console.log("Location Entered!");
                                break;
                            case 8:
                                console.log("Points Earned");
                                var amountPaid: number = response.amountPaid;
                                if (response.userId == undefined) {
                                    var pointsEarned = Math.round(
                                        amountPaid / 10
                                    );
                                    this.eventService.publishBillVerifiedAndPointsEarned(pointsEarned);
                                    this.openPointsEarnedModal(
                                        pointsEarned
                                    );
                                } else {
                                    this.eventService.publishBillVerifiedAndPointsEarned(pointsEarned);
                                    this.openPointsEarnedModal(
                                        pointsEarned
                                    );
                                }
                                break;
                        }

                        if (response.tap) {
                            this.toastService.presentToast(body, isGreen);
                        } else {
                            this.toastService.presentToast(body, isGreen);
                            localStorage.setItem("rootTo", "");
                        }
                    },
                    error => {
                        console.log("Error getting the notification", error);
                    }
                );
            }
        });
    }

    async openPointsEarnedModal(points: number) {
        let pointsEarnedModal = await this.modalCtrl.create({
            component: PointsEarnedPage, 
            componentProps: {
                points: points
            }
        });

        return await pointsEarnedModal.present();
    }

    async openNewLevelModal(levelId: number) {
        let newLevelModal = await this.modalCtrl.create({
            component: NewLevelPage, 
            componentProps: {
                levelId: levelId
            }
        });

        return await newLevelModal.present();
    }

    async openBadgeEarnedModal(
        newUserBadgePushResult: NewUserBadgePushResult,
    ) {
        let badgeEarnedModal = await this.modalCtrl.create({
            component: "BadgeEarnedPage", 
            componentProps: {
                newUserBadgePushResult: newUserBadgePushResult,
            }
        });

        return await badgeEarnedModal.present();
    }

    async openMessageModal(
        venueName: string,
        message: string
    ) {
        let messageModal = await this.modalCtrl.create({
            component: DirectMessagePage, 
            componentProps: {
                venueName: venueName,
                message: message
            }
        });

        return await messageModal.present();
    }

    async openRewardClaimedModal(
    ) {
        let rewardClaimedModal = await this.modalCtrl.create({
            component: RewardClaimedPage
        });

        return await rewardClaimedModal.present();
    }
}
