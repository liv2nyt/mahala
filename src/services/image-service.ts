import { Injectable } from '@angular/core';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { Headers } from '@angular/http';
import { Platform, LoadingController } from '@ionic/angular';
import { ToastService } from './toast-service';
import { CacheService } from './cache-service';
import { ServiceResult, ImageResult } from 'src/contracts/contracts';

@Injectable()

export class ImageService {

    public options: CameraOptions = {
        allowEdit: true,
        sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
        mediaType: this.camera.MediaType.ALLMEDIA,
        destinationType: this.camera.DestinationType.FILE_URI
    }
    file: any = "";
    headers: any;

    constructor(
        private toastService: ToastService,
        private camera: Camera,
        private transfer: FileTransfer,
        private crop: Crop,
        private platform: Platform,
        private loadingCtrl: LoadingController,
        private cacheService: CacheService) {
    }

    uploadImage(url, status) {
        return new Promise(async (resolve, reject) => {
            var loading = await this.loadingCtrl.create({
                message: 'Loading ...'
            });

            loading.present();

            this.camera.getPicture(this.options)
                .then((fileUri) => {
                    if (this.platform.is('ios')) {
                        return fileUri
                    } else if (this.platform.is('android')) {
                        // Modify fileUri format, may not always be necessary
                        fileUri = 'file://' + fileUri;
                        /* Using cordova-plugin-crop starts here */
                        return this.crop.crop(fileUri, { quality: 100 });
                    }
                })
                .then((path) => {
                    console.log('Cropped Image Path!: ' + path);
                    this.file = path;
                    return path;
                })
                .then(() => {
                    var tempFileName = this.file;
                    var targetPath = '';
                    var filename = '';

                    if (this.platform.is('ios')) {
                        targetPath = this.file;
                        filename = this.cacheService.getUserId().toString();
                    }
                    else {
                        targetPath = tempFileName.substring(0, tempFileName.lastIndexOf("?"));
                        filename = targetPath.substring(this.file.lastIndexOf("/") + 1, this.file.length + 1);
                    }

                    this.headers = new Headers();
                    var authToken = this.cacheService.getBearerToken();

                    this.headers.append('Authorization', "Bearer " + authToken);

                    var options: FileUploadOptions = {
                        fileKey: "file",
                        fileName: filename,
                        headers: this.headers
                    };

                    const fileTransfer: FileTransferObject = this.transfer.create();

                    // Use the FileTransfer to upload the image
                    fileTransfer.upload(targetPath, url, options, true).then((data: any) => {
                        var result = JSON.parse(data.response);
                        var response = result.Data;
                        this.toastService.presentToast(status + " Image Uploaded Successfully", true);
                        loading.dismiss();
                        resolve(response);
                    }, err => {
                        loading.dismiss();
                        reject("Image upload failed");
                        console.log("Error uploading file : " + err.toString());
                    }).catch(error => {
                        loading.dismiss();
                        console.log(error);
                        reject("Image upload failed");
                    });
                }).catch(error => {
                    loading.dismiss();
                    console.log(error);
                    reject("Image not selected");
                });
        });
    }
}