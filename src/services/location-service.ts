import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
// import { Platform } from 'ionic-angular/platform/platform';
import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { CacheService } from './cache-service';
import { Constants } from '../contracts/constants';
import { Platform } from '@ionic/angular';
import { ToastService } from './toast-service';

@Injectable()

export class LocationService {

    constructor(
        public diagnostic: Diagnostic,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public geolocation: Geolocation,
        private locationAccuracy: LocationAccuracy,
        private cacheService: CacheService,
        private toastService: ToastService) {
    }

    getUserLocation() {
        return new Promise(async (resolve, reject) => {
            var loading = await this.loadingCtrl.create({
                message: 'Retrieving Location....'
            });

            ;
            if (localStorage.getItem("homepageReload") == "1") {
                localStorage.setItem("homepageReload", "0");
            } else {
                await loading.present();
            }

            await this.geolocation.getCurrentPosition().then((position) => {
                loading.dismiss();
                this.cacheService.setCacheLocationPreference(Constants.LOCATION_PREFERENCE_GPS);
                localStorage.setItem('Latitude', position.coords.latitude.toString());
                localStorage.setItem('Longitude', position.coords.longitude.toString());
                localStorage.setItem("AppConfigured", "True");
                // resolve();
                resolve();
            }).catch((error) => {
                loading.dismiss();
                console.log('Error getting location', error);
                // reject();
                reject();
            });
        });
    }

    isDeviceLocationEnabled(){
        return new Promise(async (resolve, reject) => {
            await this.diagnostic.isLocationEnabled().then(
                (isAvailable) => {
                    switch (isAvailable) {
                        case true:
                            resolve(true);
                            break;
                        case false:
                            reject(false);
                            break;
                        default:
                            break;
                    }
                })
                .catch(() => {
                    reject();
                });
        });
    }

    requestLocationAuthorization() {
        return new Promise(async (resolve, reject) => {
            await this.diagnostic.requestLocationAuthorization("when_in_use").then(response => {
                switch (response) {
                    case "GRANTED":
                        resolve(true);
                        break;
                    case "DENIED":
                        resolve(false);
                        break;
                    case "authorized_when_in_use":
                        resolve(true);
                        break;
                    case "denied":
                        resolve(false);
                        break;
                    default:
                        resolve(false)
                        break;
                }
            })
                .catch(() => {
                    reject();
                });
        });
    }

    isLocationAuthorized() {
        return new Promise(async (resolve, reject) => {
            await this.diagnostic.isLocationAuthorized()
            .then(response => {
                resolve(response);
                // if (response) {
                //     resolve(true);
                // }
                // else {
                //     resolve(false);
                // }
            })
            .catch(() => {
                reject();
            }); 
        });
    }

    getGpsAvailability() {
        return new Promise(async (resolve, reject) => {
            await this.diagnostic.isLocationAvailable().then(response => {
                switch (response) {
                    case true:
                        resolve(true);
                        break;
                    case false:
                        resolve(false);
                        break;
                    default:
                        break;
                }
            })
                .catch(() => {
                    reject();
                });
        });
    }

    getLocationAvailability() {
        return new Promise(async (resolve, reject) => {
            this.platform.ready().then(() => {
                this.isLocationAuthorized().then(
                    response => {
                        if (response) {
                            this.getGpsAvailability().then(response => {
                                if (response) {
                                    console.log("GPS Available");
                                    this.getUserLocation().then(() => {
                                        console.log("Location Retrieved");
                                        resolve();
                                    },
                                        () => {
                                            console.log("Location Not Retrieved");
                                            reject();
                                        });
                                }
                                else {
                                    console.log("GPS Not Available");
                                    reject();
                                }
                            },
                                () => {
                                    console.log("GPS Not Available Error");
                                    this.cacheService.setCacheLocationPreference("");
                                    reject();
                                });
                        }
                        else {
                            this.requestLocationAuthorization().then(response => {
                                if (response) {
                                    console.log("Location Authorized");
                                    this.getGpsAvailability().then(response => {
                                        if (response) {
                                            console.log("GPS Available");
                                            this.getUserLocation().then(() => {
                                                console.log("Location Retrieved");
                                                this.toastService.presentToast("We have successfully located your device", true);
                                                resolve();
                                            },
                                                () => {
                                                    console.log("Location Not Retrieved");
                                                    reject();
                                                });
                                        }
                                        else {
                                            console.log("GPS Not Available");
                                            reject();
                                        }
                                    },
                                        () => {
                                            console.log("GPS Not Available Error");
                                            this.cacheService.setCacheLocationPreference("");
                                            reject();
                                        })
                                        .catch((error) => {

                                        });
                                }
                                else {
                                    console.log("Location Not Authorized");
                                    reject();
                                }

                            },
                                () => {
                                    reject();
                                })
                                .catch((error) => {

                                });
                        }
                    });
            })
        });
    }

    validateLocationSettings() {
        return new Promise(async (resolve, reject) => {
            var locationPreference = this.cacheService.getCachedLocationPreference();

            if (locationPreference == null || locationPreference == "" || locationPreference == undefined) {
                reject();
            }

            switch (locationPreference) {
                case "GPS":
                    this.getLocationAvailability()
                        .then(() => {
                            if (this.platform.is('ios')) {
                                resolve();
                            }
                            else {
                                this.locationAccuracy.canRequest()
                                    .then((canRequest: boolean) => {
                                        if (canRequest) {
                                            // the accuracy option will be ignored by iOS
                                            this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                                                () => {
                                                    resolve();
                                                },
                                                error => {
                                                    resolve();
                                                    console.log('Error requesting location permissions', error);
                                                }
                                            );
                                        }
                                        else {
                                            resolve();
                                        }
                                    }).catch(() => {
                                        reject();
                                    });;
                            }
                        })
                        .catch(() => {
                            reject();
                        });

                    break;
                case "Address":
                    var latitude = localStorage.getItem("Latitude");

                    if (latitude != null || latitude != "") {
                        var address = localStorage.getItem("Address");
                        if (address != null) {
                            // this.toastService.presentToast("Viewing events from: " + address, true);
                        }
                        resolve();
                    }
                    else {
                        reject();
                    }
                    break;
                default:
                    console.log("Unknown Checking Location Settings Error");
                    break;
            }
        });
    }

    confirmLocation(lat: number, long: number) {
        return new Promise(async (resolve, reject) => {
            this.validateLocationSettings()
                .then(() => {
                    var locationPreference = this.cacheService.getCachedLocationPreference();
                    if (locationPreference != Constants.LOCATION_PREFERENCE_ADDRESS) {
                        this.getUserLocation().then(() => {
                            var latitude = localStorage.getItem('Latitude');
                            var longitude = localStorage.getItem('Longitude');
                            var distanceBetweenLocationsInMeters = this.getDistanceFromLatLonInMeters(latitude, longitude, lat, long);
                            if (distanceBetweenLocationsInMeters >= Constants.DISTANCE_BETWEEN_POINTS) {
                                resolve(false);
                            }
                            else {
                                resolve(true);
                            }
                        })
                        .catch((error) => {
                            // reject();
                        });
                    }
                    else {
                        reject();
                    }
                })
                .catch(() => {
                    reject();
                });
        });
    }

    getDistanceFromLatLonInMeters(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
        var dLon = this.deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
            ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        var m = d * 1000; //Distance in meters
        return m;
    }

    deg2rad(deg) {
        return deg * (Math.PI / 180)
    }
}