import { FormControl } from '@angular/forms';
 
export class EmailValidator {
 
    static isEmailValid(control: FormControl): any {

        if(!control.value)
        {
            return null;
        }

        if(control.value.indexOf("@") == -1){
            return {
                "Invalid Email Address": true
            };
        }
        if(control.value.indexOf(".") == -1){
            return {
                "Invalid Email Address": true
            };
        }
 
        return null;
    }
 
}