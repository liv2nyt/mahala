export class Constants {
    static readonly LOCAL: string = 'http://localhost:46362/api';
    static readonly DEV: string = 'https://mahalaapi.ambitiondev.com/api';
    static readonly PROD: string = 'http://linkapi.ambitiondev.com/api';
    static readonly ENVIRONMENT: string = Constants.DEV;
    static readonly DEBUG: boolean = true;
    static readonly LOCATION_PREFERENCE_ADDRESS: string = 'Address';
    static readonly LOCATION_PREFERENCE_GPS: string = 'GPS';
    static readonly DISTANCE_BETWEEN_POINTS: number = 50;
    static readonly REGISTER_COMPLETION_EVENT: string = "REGISTER_COMPLETION_EVENT";
    static readonly SET_ROOT_WALKTHROUGH: string = "SET_ROOT_WALKTHROUGH";
    static readonly SET_ROOT_FACEBOOK_REGISTRATION: string = "SET_ROOT_FACEBOOK_REGISTRATION";
    static readonly LOGIN_SUCCESSFUL: string = "LOGIN_SUCCESSFUL";
    static readonly REGISTRATION_SUCCESSFUL: string = "REGISTRATION_SUCCESSFUL";
    static readonly FACEBOOK_LOGIN_SUCCESSFUL: string = "FACEBOOK_LOGIN_SUCCESSFUL";
    static readonly FACEBOOK_REGISTRATION_SUCCESSFUL: string = "FACEBOOK_REGISTRATION_SUCCESSFUL";
    static readonly SET_ROOT_LANDING_PAGE_MODAL: string = "SET_ROOT_LANDING_PAGE_MODAL";
    static readonly PHOTO_VIEWER_OPTIONS = {
        share: true, 
        closeButton: true,
        copyToReference: true,
        headers: '', 
        piccasoOptions: { }
    };
};