import { Injectable } from '@angular/core';
// import { DateTime } from '@ionic/angular';

@Injectable()
export class ServiceResult<T> {
    WasSuccessful: boolean;
    Heading: string;
    Description: string;
    Data: T;
}
//Random obj - 
export class Continue {
    order: OrderResult[];
    makePayment: boolean;
    navToBill: boolean;
}

//App
export class AppVersionResult {
    Version: string;
    IOSLink: string;
    AndroidLink: string;
}

// Recommendations
export class RecommendationResult {
    Id: number;
    RecommendationUser: RecommendationUser;
    Recommended: boolean;
    Review: string;
    ReviewAddedDate: string;
    ReviewAddedTime: string;
    Image: string;
    TinyImage: string;
    ListImage: string;
    MediumImage: string;
    RecommendationReply: RecommendationReply;
}

export class RecommendationUser {
    Id: number;
    FirstName: string;
    LastName: string;
    ListImage: string;
    Review: string;
}

export class RecommendationReply {
    Reply: string;
    ReplyAddedDate: string;
    ReplyAddedTime: string;
}


// Trophy Room, Badge, UserRewards
export class TrophyRoom {
    Points: number;
    CurrentLevel: Level;
    NextLevel: Level;
    UserBadges: UserBadge[];
    Rewards: Reward[];
    LeaderBoards: LeaderBoard[];
}

export class UserBadge {
    Id: number;
    Gained: boolean;
    BadgeId: number;
    Badge: Badge;
    UserId: number;
    User: User;
    VenueId: number;
    Venue: VenueResult;
}

export class Badge {
    Id: number;
    Name: string;
    Description: string;
    Color: string;
    BadgeTypeId: number;
    BadgeType: BadgeType;
}

export class BadgeType {
    Id: number;
    Name: string;
    Description: string;
}

export class BadgeLevel {
    Id: number;
    Name: string;
    Level: number;
}

export class UserBadgeResult {
    Id: number;
    Name: string;
    Description: string;
    BadgeTypeId: number;
    Color: string;
}

export class NewUserBadgePushResult {
    NewUserBadgeResult: UserBadgeResult;
    Venue: SearchVenueResult;
}

export class ClaimUserRewardRequest {
    RewardId: number;
    VenueId: number;
}

export class UserReward {
    Id: number;
    UserId: number;
    User: User;
    RewardId: number;
    Reward: Reward;
    QrCode: string;
    Claimed: boolean;
    Active: boolean;
}

export class UserRewardResult {
    RewardId: number;
    Description: string;
    BillUserId: number;
    BillUserFullName: string;
    MenuItemId: number;
    MenuItem: MenuItem; 
}

export class Reward {
    Id: number;
    Description: string;
    MenuItemId: number;
    MenuItem: MenuItem;
    LeveId: number;
    Level: Level;
    PointsNeeded: number;
    VenueId: number;
    Venue: VenueResult;
    Active: boolean;
}

export class Level {
    Id: number;
    Name: string;
    Description: string;
    MinPoints: number;
    BadgeRequirements: number[];
}

export class LeaderBoard {
    LeaderBoardTypeId: number;
    LeaderBoardResult: LeaderBoardUserResult[];
}

export class LeaderBoardType {
    Id: number;
    Description: string;
}

export class LeaderBoardUserResult {
    Number: number;
    User: User;
    Level: Level;
    Points: number;
}

//Card
export class CardResult {
    CardId: number;
    CardNumber: string;
    CardTypeId: number;
    ExpiryDate: string;
}

export class CardType {
    Id: number;
    Description: string;
}

export class AddCardResult {
    CardId: number;
    Reference: string;
}

export class AddCardResolve {
    AddCardRequest: AddCardRequest;
    CardId: number;
}

//Login
export class UserNameLogin {
    Username: string;
    Password: string;
}
export class EmailLogin {
    EmailAddress: string;
    Password: string;
}
export class FacebookLogin {
    EmailAddress: string;
    FacebookId: string;
}
export class LoginResult {
    UserId: number;
    Username: string;
    FirstName: string;
    LastName: string;
    EmailAddress: string;
    Gender: string;
    MobileNumber: string;
    LoginCount: number;
    Token: string;
    Image: string;
    TinyImage: string;
    MediumImage: string;
    UserRoleId: number;
    VenueId?: number;
    Password: string;
    FacebookId?: string;
    FacebookAuthToken?: string;
}

// Enums
export enum OrderStatusEnum {
    Pending = "Pending",
    Confirmed = "Confirmed",
    Rejected = "Rejected"
}

//Card
export class AddCardRequest {
    CardNumber: string;
    NameOnCard: string;
    ExpiryDate: string;
    CardTypeId: number;
    Cvv: string;
}

export class CardTypeResult {
    Id: number;
    Description: string;
}

//Venue
export class VenueResult {
    Id: number;
    Name: string;
    About: string;
    Description: string;
    Phone: string;
    Website: string;
    Image: string;
    TinyImage: string;
    ListImage: string;
    SingleLineAddress: string;
    Views: number;
    Followers: number;
    Following: boolean;
    TableJoined: boolean;
    IsHoursAvailable: boolean;
    RecommendedCount: number;
    NotRecommendedCount: number;
    Distance: string;
    Duration: string;
    CheckedIn: boolean;
    Location: LocationResult;
    Hours: Hours;
    VenueTypes: number[];
    FriendsWhoVisitedVenue: FriendsResult;
}

export class LocationResult {
    Id: number;
    City: string;
    Country: string;
    Latitude: number;
    Longitude: number;
    Street: string;
    Zip: string;
    Region: string;
    State: string;
}

export class ImageResult {
    Image: string;
    TinyImage: string;
    ListImage: string;
    MediumImage: string;
}

export class FriendsResult {
    UserId: number;
    FirstName: string;
    ListImage: number;
}


//User Notifications
export class UserNotificationResult {
    FriendRequestCount: number;
    InvitationCount: number;
    Bill: BillUserOrdersResult;
    TableRequests: TableRequestResult[];
}

export class BillUserOrdersResult {
    Id: number;
    Count: number;
    Table: Table;
    PaymentMade: boolean;
    TableJoined: boolean;
    Total: number;
    DateTime: Date;
    InvoiceNo: number;
    PointsEarned: number;
    Orders: OrderResult[];
    BillUsers: BillUser[];
    Venue: SearchVenueResult;
    TableRequests: TableRequestResult[];
    UserRewards: UserRewardResult[];
}

export class SearchVenueResult {
    Id: number;
    Name: string;
    SingleLineAddress: string;
    ListImage: string;
}

export class TableRequestResult {
    Id: number;
    FromUserId: number;
    FromUser: User;
    ToUserId: number;
    ToUser: User;
    Status: string;
    BillId: number;
    Bill: Bill;
}

export class TableRequestPushResult {
    Accepted: boolean;
    FriendFullName: string;
    VenueName: string;
    TableRequestId: number;
}

export class TableConfirmationRequestPushResult {
    Accepted: boolean;
    VenueName: string;
    TableConfirmationId: number;
}

// Bill, Menu, Order, Waiter Results
export class AddToBill {
    Orders: OrderResult[];
    VenueId: number;
    FirstOrder: boolean;
    BillOrdersResult?: BillUserOrdersResult;
    Joined: boolean;
    OrderedByWaiter: boolean;
    UpdateStatus: boolean;
    OrdersPushResult: OrdersPushResult;
}

export class OrdersPushResult {
    BillUser: BillUserPushResult;
    Orders: OrderPushResult[];
    BillId: number;
}

export class BillUserPushResult {
    Id: number;
    FirstName: string;
    LastName: string;
}

export class OrderPushResult {
    Id: number;
    Count: number;
    MenuItemId: number;
}

export class OrderResult {
    Id: number;
    Count: number;
    MenuItemId: number;
    MenuItem: MenuItem;
    BillUserId: number;
    BillUser: BillUser;
    OrderedByWaiter: boolean;
    Status: string;
}

export class MenuItem {
    Id: number;
    Name: string;
    Description: string;
    Price: number;
    MenuId: number;
    MenuItemTypeId: number;
    MenuItemSectionId: number;
    MenuItemSubSectionId: number;
    Priority: number;
    Active: boolean;
    Image: string;
    TinyImage: string;
    ListImage: string;
}

export class MenuItemType {
    Id: number;
    Name: string;
    Description: string;
    MenuId: number;
    Menu: Menu;
}

export class MenuItemSection {
    Id: number;
    Name: string;
    Description: string;
    MenuItemTypeId: number;
}

export class MenuItemSubSection {
    Id: number;
    Name: string;
    Description: string;
    MenuItemSectionId: number;
}

export class MenuResult {
    Id: number;
    Name: string;
    Description: string;
    VenueId: number;
}

export class BillUser {
    Id: number;
    UserId: number;
    User: User;
    BillId: number;
    Bill: Bill;
    PaymentId: number;
    Payment: Payment;
}

export class User {
    Id: number;
    FirstName: string;
    LastName: string;
    Gender: string;
    EmailAddress: string;
    Image: string;
    TinyImage: string;
    ListImage: string;
}

export class Bill {
    Id: number;
    InvoiceNo: number;
    TableId: number;
    Table: Table;
    PaymentMade: boolean;
    Total: number;
    Rating: number;
    VenueId: number;
    BillSplit: boolean;
}

export class Payment {
    Id: number;
    Amount: number;
    UserId: number;
    User: User;
    PaymentTypeId: boolean;
    PaymentType: PaymentType;
}

export class PaymentType {
    Id: number;
    Name: string;
    Description: string;
}

export class PaymentRequest {
    BillId: number;
    Tip: number;
    PaymentTypeId: number; 
}

export class MakePayment {
    BillId: number;
    Amount: number;
    Cvv: number;
    Tip: number;
    CardId: number;
}

export class MakePaymentResult {
    Reference: string;
    PointsEarned: number;
    NewLevelId?: number;
}

export class MakePaymentResolve {
    MakePaymentResult: MakePaymentResult;
    WasSuccessful: boolean;
}

export class Table {
    Id: number;
    Number: number;
    WaiterId?: number;
    Waiter?: Waiter;
    Active: boolean;
    TableSectionId: boolean;
    TableSection: TableSection;
}

export class TableSection {
    Id: number;
    Name: string;
    Description: string;
    VenueId: number;
}

export class UserAssignedTablesResult {
    Sections: SectionTablesResult;
}

export class SectionTablesResult {
    Id: number;
    Name: string;
    Description: string;
    AssignedTables: AssignedTablesResult[];
}

export class AssignedTablesResult {
    Id: number;
    TableNo: number;
}

export class Waiter {
    Id: number;
    FiestName: string;
    LastName: string;
    EmailAddress: string;
    Image: string;
    TinyImage: string;
    ListImage: string;
    Rating: number;
}

export class _Menu {
    Id: number;
    MenuItemTypeResults: MenuItemTypeResult[];
}

export class MenuItemTypeResult {
    Id: number;
    Name: string;
    Description: string;
    MenuItemSectionResults: MenuItemSectionResult[];
}

export class MenuItemSectionResult {
    Id: number;
    Name: string;
    Description: string;
    MenuItemSubSectionResults: MenuItemSubSectionResult[];
    MenuItems: MenuItem[];
}

export class MenuItemSubSectionResult {
    Id: number;
    Name: string;
    Description: string;
    MenuItems: MenuItem[];
}

//The rest
export class Menu {
    FoodItems: Sections[];
    DrinkItems: Sections[];
}

export class Sections {
    id: number;
    Title: string;
    Description: string;
    Subtitle: itemSubtitle[];
}

export class itemSubtitle {
    Subtitle: string;
    SectionItems: Item[];
}

export class Item {
    itemName: string;
    itemDescription: string;
    itemPrice: number;
    itemId: number;
}

export class SpotDistance {
    Page: number;
    Latitude: number;
    Longitude: number;
    Radius: number;
}

export class SpotDistanceFilter {
    Page: number;
    TakeCount: number;
    Latitude: number;
    Longitude: number;
    Radius: number;
    VenueTypes: number[];
}

export class FriendsFeedRequest {
    Page: number;
    Latitude: number;
    Longitude: number;
}

export class FriendsVenuesDistanceResult {
    FriendsVenuesDistanceUser: FriendsVenuesDistanceUser;
    FriendsVenuesDistanceVenue: FriendsVenuesDistanceVenue;
    FriendsVenuesDistanceTable: FriendsVenuesDistanceTable;
    Distance: number;
}

export class FriendsVenuesDistanceUser {
    Id: number;
    FirstName: string;
    LastName: string;
    Image: string;
    TinyImage: string;
}

export class FriendsVenuesDistanceVenue {
    Id: number;
    Name: string;
    Latitude: number;
    Longitude: number;
}

export class FriendsVenuesDistanceTable {
    Id: number;
    Number: number;
    BillId: number;
    Joined: boolean;
    TableRequest: TableRequestStatusResult;
}

export class TableConfirmationResult {
    Id: number;
    UserId: number;
    Status: string;
    TableId: number;
    VenueId: number;
}

export class TableRequestStatusResult {
    Status: string;
}

export class VenueType {
    Id: number;
    Description: string;
}

export class PushNotificationType {
    Id: number;
    Description: string;
}

export class Time {
    Open: string;
    Close: string;
}

export class Day {
    DayOfWeek: number;

    Times: Time[];
}

export class Hours {
    Days: Day[];
}

export class InvitationDetails {
    UserList: string;
    GroupList: string;
    VenueId: number;
    StartDateTime: Date;
    EndDateTime: Date;
    Message: string;
}

export class AddFriend {
    UserId: number;
}

export class AddGroup {
    GroupId: number;
}

export class PartyInvitationDetails {
    UserList: string;
    PartyId: number;
}