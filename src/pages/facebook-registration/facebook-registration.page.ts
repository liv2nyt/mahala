import { Component } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { IdentityService } from '../../services/identity-service'
import { Http, Headers } from '@angular/http';
// import { Keyboard } from '@ionic-native/keyboard';
import { ApiService } from '../../services/api-service';
import { CacheService } from '../../services/cache-service';
import { NotificationService } from '../../services/notification-service';
import { ImageService } from 'src/services/image-service';
import { ImageResult } from 'src/contracts/contracts';

@Component({
  selector: 'facebook-registration',
  templateUrl: './facebook-registration.page.html',
	styleUrls: ['./facebook-registration.page.scss']
})
export class FacebookRegistrationPage {
  registrationForm: FormGroup;
  submitAttempt: boolean = false;
  step: string = '1';
  countryCode: string = "+27";
  mobileSubmitAttempt: boolean = false;
  imageUploadedSuccess: boolean = false;
  imageUrl: any = "assets/imgs/profile-imgs/user.svg";
  tinyImageUrl: any = "assets/imgs/profile-imgs/user.svg";

  constructor(
    // private keyboard: Keyboard,
    public _http: Http,
    public imageService: ImageService,
    public alertCtrl: AlertController,
    public identityService: IdentityService,
    public formBuilder: FormBuilder,
    private userProvider: ApiService,
    private modalCtrl: ModalController,
    private cacheService: CacheService,
    private notificationService: NotificationService) {
    this.initializeForm();
  }

  initializeForm() {
    let checkUsername = (control: FormControl): any => {
      var headers = new Headers();
      headers.append('Content-Type', 'application/json; charset=utf-8');

      return new Promise((resolve) => {
        var usernameToCheck = control.value.toLowerCase()
        this.userProvider.IsUsernameAvailable(usernameToCheck).then(() => {
          resolve(null);
        })
          .catch(() => {
            resolve({
              "username taken": true
            });
          });
      });
    }

    var user = this.cacheService.getCachedUser();

    this.registrationForm = this.formBuilder.group({
      Username: ['', Validators.compose([Validators.minLength(2), Validators.required]), checkUsername],
      Gender: ['Female'],
      CellNumber: ['', Validators.compose([Validators.minLength(9)])],
      Image: [user.Image],
      EmailAddress: [user.EmailAddress],
      FacebookId: [user.FacebookId],
      FacebookAuthToken: [user.FacebookAuthToken],
      FirstName: [user.FirstName],
      LastName: [user.LastName]
    });
  }

  goBack(data) {
    this.modalCtrl.dismiss(data);
  }

  // closeKeyboard() {
  //   this.keyboard.hide();
  // }

  async goToNextStep() {

    this.submitAttempt = true;
    switch (this.step) {
      case '1':
        if (!this.registrationForm.controls.Username.valid) {
          const alert = await this.alertCtrl.create({
            header: 'Incomplete Form',
            message: 'Please complete all the fields correctly and then try again',
            buttons: ['OK']
          });
          return await alert.present();
        } else {
          this.step = '2';
        }
        break;
      case '2':
        this.mobileSubmitAttempt = true;
        if (!this.registrationForm.controls.CellNumber.valid || this.countryCode == "" || this.registrationForm.value.CellNumber == "") {
          const alert = await this.alertCtrl.create({
            header: 'Invalid Mobile Number',
            message: 'Please ensure you have correctly entered your mobile number and try again',
            buttons: ['OK']
          });
          return await alert.present();
        } else {
          if (this.countryCode != null && this.countryCode.substring(0, 1) != '+') {
            var countryCodeStr = ""
            countryCodeStr = '+' + this.countryCode;
            this.countryCode = countryCodeStr;
          }
          if (this.registrationForm.value.CellNumber != null && this.registrationForm.value.CellNumber.substring(0, 1) == "0") {
            var mobileNumberStr = "";
            mobileNumberStr = this.registrationForm.value.CellNumber.substring(1, this.registrationForm.value.CellNumber.length)
            this.registrationForm.value.CellNumber = mobileNumberStr;
          }
          this.registerUser();
        }
        break;
      case '3':
        this.goBack(true);
        break;
    }
  }

  uploadImage() {
    this.imageService.uploadImage(this.userProvider.url + "/Image/User", "Facebook Profile")
    .then((response: ImageResult) => {
      var user = this.cacheService.getCachedUser();
      user.Image = response.Image;
      user.TinyImage = response.TinyImage;
      user.MediumImage = response.MediumImage;
      this.cacheService.setCachedUser(user);
      this.goBack(true);
    }).catch(error => {
      console.log(error);
      this.notificationService.presentAlertWithDescription('Failed to Upload Profile Image', "The image failed to upload, please ensure you have an internet connection and try again");
    })
  }


  cancel() {
    switch (this.step) {
      case '1':
        this.goBack(false);
        break;
      case '2':
        this.step = '1';
        break;
    }
  }

  skipStep() {

    switch (this.step) {
      case '2':
        this.registerUser();
        break;
      case '3':
        this.goBack(true);
        break;
    }
  }

  registerUser() {
    if (this.registrationForm.value.CellNumber != null && this.registrationForm.value.CellNumber.substring(0, 1) != '+') {
      this.registrationForm.value.CellNumber = this.countryCode + this.registrationForm.value.CellNumber;
    } else {
      this.registrationForm.value.CellNumber = null;
    }

    if (this.registrationForm.controls.CellNumber.value != null || this.registrationForm.controls.CellNumber.value != undefined || this.registrationForm.controls.CellNumber.value != '') {
      this.registrationForm.value.CellNumber = this.countryCode + this.registrationForm.controls.CellNumber.value;
    }

    this.registrationForm.value.Username.trim();

    this.identityService.registerExternalUserV2(this.registrationForm.value)
      .then(() => {
        this.step = '3';
      });
  }
}
