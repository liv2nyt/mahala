import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FacebookRegistrationPage } from './facebook-registration.page';

const routes: Routes = [
  {
    path: "",
    component: FacebookRegistrationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FacebookRegistrationPage]
})
export class FacebookRegistrationPageModule {}


