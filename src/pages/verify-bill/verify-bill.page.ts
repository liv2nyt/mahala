import { Component } from "@angular/core";
import { AlertController, ModalController } from "@ionic/angular";
import { EventService } from 'src/services/event-service';

@Component({
    templateUrl: './verify-bill.page.html',
    selector: 'verify-bill',
    styleUrls: ['./verify-bill.page.scss']
})
export class VerifyBillPage {
    userId: number;
    venueId: number;
    qrCode: string;

    constructor(
        private modalCtrl: ModalController,
        private alertCtrl: AlertController,
        private eventService: EventService) {
    }

    ionViewWillEnter() {
        this.qrCode = this.userId + ',' + this.venueId;
        this.initBillVerified();
    }

    initBillVerified() {
        // this.events.subscribe('billVerified', () => {
        //     var alert = this.alertCtrl.create({
        //         title: "Bill Verified Claimed",
        //         subTitle: "Your bill was verified successfully",
        //         buttons: [{
        //             text: "Ok",
        //             handler: () => {
        //                 this.goBack(true);
        //             }
        //         }]
        //     });
        //     alert.present();
        // });

        this.eventService.billVerifiedAndPointsEarnedSource$.subscribe(pointsEarned => {
            this.goBack(pointsEarned);
        });
    }

    goBack(pointsEarned) {
        this.modalCtrl.dismiss(pointsEarned);
    }
}