import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angular2-qrcode';
import { VerifyBillPage } from './verify-bill.page';

const routes: Routes = [
  {
    path: "",
    component: VerifyBillPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    QRCodeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VerifyBillPage]
})
export class VerifyBillPageModule {}


