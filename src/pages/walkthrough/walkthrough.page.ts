import { Component, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ToastService } from '../../services/toast-service';

@Component({
    selector: 'walkthrough',
    templateUrl: './walkthrough.page.html',
    styleUrls: ['./walkthrough.page.scss']
})
export class WalkthroughPage {
    constructor(
        private modalCtrl: ModalController,
        private toastService: ToastService) {
        this.toastService.presentToast("Swipe right to go next", true);
    }

    continue() {
        this.goBack();
    }

    goBack() {
        this.modalCtrl.dismiss();
    }
}
