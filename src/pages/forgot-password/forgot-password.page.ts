import { Component } from '@angular/core';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { ApiService } from '../../services/api-service';
import { NotificationService } from '../../services/notification-service';
import { ToastService } from '../../services/toast-service';

@Component({
  selector: 'forgot-password',
  templateUrl: '/forgot-password.page.html',
	styleUrls: ['./forgot-password.page.scss']
})
export class ForgotPasswordPage {
  step: string = '1';
  confirmPass: string = "";
  newPass: string = "";
  emailAddress: string = "";
  otpPin: string = "";
  UserId: string = "";
  UserTypeId: string = "";
  constructor(
    private userProvider: ApiService,
    private notificationService: NotificationService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toastService: ToastService,
    private modalCtrl: ModalController) {
  }

  async goToNextStep() {
    if (this.step == "1") {
      if (this.emailAddress.indexOf("@") != -1) {
        this.forgotPasswordNotification();
      } else {
        const confirm = await this.alertCtrl.create({
          // title: 'Please ensure you have entered a correct Email Address',
          message: 'Please ensure you have entered a correct email address',
          buttons: [
            {
              text: 'Ok'
            },
          ]
        });
        return await confirm.present();
      }

    } else if (this.step == "2") {
      this.validateOTP();
    } else {
      if (this.newPass == this.confirmPass) {
        if (this.newPass.length > 6) {
          this.resetPassword();
        } else {
          const confirm = this.alertCtrl.create({
            // title: 'The password entered is to short',
            header: 'The password entered is to short',
            buttons: [
              {
                text: 'Ok'
              },
            ]
          });
          // confirm.present();
        }

      } else {
        const confirm = await this.alertCtrl.create({
          // title: 'Passwords do not match',
          header: 'Passwords do not match',
          buttons: [
            {
              text: 'Ok'
            },
          ]
        });
        
        return await confirm.present();
      }
    }
  }

  goBackPage() {
    // this.viewCtrl.dismiss();
    this.modalCtrl.dismiss();
  }

  async forgotPasswordNotification() {

    let loading = await this.loadingCtrl.create({
      // content: 'Sending OTP...'
      message: 'Sending OTP...'
    });

    var data = {
      EmailAddress: this.emailAddress
    }

    await loading.present();

    this.userProvider.resetPasswordNotification(data).then((response: any) => {
      if (response.WasSuccessful) {
        loading.dismiss();
        this.UserId = response.Data.UserId;
        this.UserTypeId = response.Data.UserTypeId;
        this.step = "2";
      }
      else {
        loading.dismiss();
        this.notificationService.showErrorMessage(response);
      }
    })
      .catch(err => {
        loading.dismiss();
        if (err.status == 0) {
          this.notificationService.presentNoInternetAlert();
        } else {
          this.notificationService.showErrorMessage(err);
        }
      });
  }

  async validateOTP() {

    let loading = await this.loadingCtrl.create({
      // content: 'Validating OTP...'
      message: 'Validating OTP...'
    });

    var data = {
      UserId: this.UserId,
      UserTypeId: this.UserTypeId,
      Otp: this.otpPin
    }
    
    await loading.present();

    this.userProvider.validateOTP(data).then((response: any) => {
      if (response.WasSuccessful) {
        loading.dismiss();
        this.step = "3";
      }
      else {
        loading.dismiss();
        this.notificationService.showErrorMessage(response);
      }
    })
      .catch(err => {
        loading.dismiss();
        if (err.status == 0) {
          this.notificationService.presentNoInternetAlert();
        } else {
          this.notificationService.showErrorMessage(err);
        }
      });
  }

  async resetPassword() {

    let loading = await this.loadingCtrl.create({
      // content: 'Resetting Password...'
      message: 'Resetting Password...'
    });

    var data = {
      UserId: this.UserId,
      UserTypeId: this.UserTypeId,
      Password: this.newPass
    }

    await loading.present();

    this.userProvider.resetPassword(data).then((response: any) => {
      if (response.WasSuccessful) {
        loading.dismiss();
        this.goBackPage();
        this.toastService.presentToast("Password Reset Successfully", true);
      }
      else {
        loading.dismiss();
        this.notificationService.showErrorMessage(response);
      }
    })
      .catch(err => {
        loading.dismiss();
        if (err.status == 0) {
          this.notificationService.presentNoInternetAlert();
        } else {
          this.notificationService.showErrorMessage(err);
        }
      });
  }

  goBack() {
    if (this.step == "3") {
      this.step = "2";
    } else if (this.step == "2") {
      this.step = "1";
    }
  }
}