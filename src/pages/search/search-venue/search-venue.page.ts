import { Component } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { ApiService } from 'src/services/api-service';
import { NotificationService } from 'src/services/notification-service';
import { VenuePage } from 'src/pages/venue/venue.page';

@Component({
    templateUrl: './search-venue.page.html',
    selector: 'search-venue',
    styleUrls: ['./search-venue.page.scss']
})
export class SearchVenuePage {
    term = "";
    venuesFound: any;
    noVenuesFound: boolean = false;
    showInfiniteScroll: boolean = true;
    pageNumber: number = 1;
    loaded: boolean = false;
    searching: any = false;
    noInternet:boolean = false;

    constructor(
        private userProvider: ApiService,
        private modalCtrl: ModalController,
        private notificationService: NotificationService
        ) {}

    async openSpot(venueId) {
        let venueModal = await this.modalCtrl.create({
            component: VenuePage, 
            componentProps: { venueId: venueId, image: null, tinyImage: null, isModal: true }
        });
        venueModal.onDidDismiss().then(((response) => {
            if(response.data)
            {
                this.modalCtrl.dismiss(true);    
            }
        }));
        venueModal.present();
    }

    goBack() {
        this.modalCtrl.dismiss(false);
    }

    clearSearch() {
        this.term = "";
        this.venuesFound = null;
        this.noVenuesFound = false;
    }

    cancelSearch() {
        this.term = "";
        this.venuesFound = null;
        this.noVenuesFound = false;
    }

    searchVenues(searchTerm) {
        this.venuesFound = [];
        this.venuesFound = null;
        this.noVenuesFound = false;
        this.showInfiniteScroll = true;
        this.searching = true;

        this.userProvider.searchVenues(searchTerm.trim(), 1)
            .then((response: any) => {
                this.searching = false;
                if (response.WasSuccessful) {
                    this.noInternet = false;
                    this.noVenuesFound = false;
                    this.venuesFound = response.Data;
                    this.loaded = true;
                    if (response.Data.length == 0) {
                        this.showInfiniteScroll = false;
                        this.noVenuesFound = true;
                    }
                }
            })
            .catch(err => {
                this.noInternet = false;
                this.searching = false;
                this.venuesFound = null;
                if (searchTerm == "") {
                    this.noVenuesFound = false;
                } else {
                    this.noVenuesFound = true;
                }
                if (err.status == 0) {
                    this.notificationService.presentNoInternetAlert();
                    this.noInternet = true;
                }
                console.log(err);
            });
    }

    doInfinite(event) {
        var infiniteScroll = event.target;
        this.pageNumber++;
        
        this.userProvider.searchVenues(this.term, this.pageNumber)
            .then((response: any) => {
                infiniteScroll.complete();
                if (response.WasSuccessful) {
                    this.loaded = true;
                    this.noVenuesFound = false;
                    if (response.Data.length < 5) {
                        this.showInfiniteScroll = false;
                    }

                    if (response.Data.length == 0) {
                        this.showInfiniteScroll = false;
                        this.noVenuesFound = true;
                    }

                    this.venuesFound.push(...response.Data);
                }

            })
            .catch(err => {
                infiniteScroll.complete();
                if (err.status == 0) {
                    this.notificationService.presentNoInternetAlert();
                }
                console.log(err);
            });
    }
}