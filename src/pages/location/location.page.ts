import { Component, ViewChild, OnInit } from '@angular/core';
import { AlertController, LoadingController, Platform, ModalController, NavParams, IonContent, NavController } from '@ionic/angular';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
// import { Keyboard } from '@ionic-native/keyboard';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { ApiService } from 'src/services/api-service';
import { LocationService } from 'src/services/location-service';
import { NotificationService } from 'src/services/notification-service';
import { ToastService } from 'src/services/toast-service';
import { CacheService } from 'src/services/cache-service';
import { Constants } from 'src/contracts/constants';
// import { NavParams } from 'ionic-angular/navigation/nav-params';

@Component({
    templateUrl: './location.page.html',
    selector: 'location',
    styleUrls: ['/location.page.scss']
})
export class LocationPage implements OnInit {
    // @ViewChild('content',null) content: any;
    @ViewChild(IonContent, null) content: IonContent;
    term = "";
    address = "";
    showButtons: boolean = false;
    addressPredictions = [];
    addressSaved: string = localStorage.getItem("Address");
    addressSelected: boolean = true;
    showEdit = true;
    appConfigured = localStorage.getItem("AppConfigured");
    locationPreference: string;
    lat: any = localStorage.getItem("Latitude");
    long: any = localStorage.getItem("Longitude");
    useGps: any = this.locationPreference == "GPS" ? true : false;
    useAddress: any = this.locationPreference == "Address" ? true : false;
    radiusChanged: boolean = false;
    hardBackPress: boolean = false;
    distanceRadius: number = 100;
    userAddressBook: any[] = [];
    showMap: boolean = false;
    initOpen: boolean = false;
    loading: boolean = false;
    openAsProvider: boolean = false;

    constructor(
        private userProvider: ApiService,
        private alertCtrl: AlertController,
        private locationAccuracy: LocationAccuracy,
        private locationService: LocationService,
        private diagnostic: Diagnostic,
        private loadingCtrl: LoadingController,
        private platform: Platform,
        private notificationService: NotificationService,
        private toastService: ToastService,
        private modalCtrl: ModalController,
        private navParams: NavParams,
        private cacheService: CacheService,
        private navCtrl: NavController
        ) {
    }

    refreshLocation() {
        if (this.platform.is('ios')) {
            this.gpsFirstRun(true);
        }
        else {
            this.locationAccuracy.canRequest().then((canRequest: boolean) => {
                if (canRequest) {
                    // the accuracy option will be ignored by iOS
                    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                        () => {
                            this.gpsFirstRun(true);
                        },
                        error => {
                            this.cacheService.setCacheLocationPreference("");
                            this.useGps = false;
                            this.useAddress = true;
                            console.log('Error requesting location permissions', error);
                        }
                    );
                } else {
                    this.gpsFirstRun(true);
                }
            });
        }
    }

    init() {
        this.lat = localStorage.getItem("Latitude");
        this.long = localStorage.getItem("Longitude");
        this.locationPreference = this.cacheService.getCachedLocationPreference();


        if (this.addressSaved == undefined && this.locationPreference != 'GPS') {
            this.initOpen = true;
        }

        if (localStorage.getItem("DistanceRadius") != null) {
            this.distanceRadius = +localStorage.getItem("DistanceRadius");
        };

        if (localStorage.getItem("AddressBook") != undefined && localStorage.getItem("AddressBook") != "") {
            this.userAddressBook = JSON.parse(localStorage.getItem("AddressBook"));
        }

        if (localStorage.getItem("venueTypes") == undefined && localStorage.getItem("venueTypes") == null) {
            this.userProvider.initializeVenueTypes();
        }

        if (this.locationPreference == Constants.LOCATION_PREFERENCE_ADDRESS) {
            this.useAddress = true;
            this.useGps = false;
        }
        if(this.locationPreference == Constants.LOCATION_PREFERENCE_GPS)
        {
            this.useGps = true;
            this.useAddress = false;
        }
    }

    ngOnInit() {
        // this.openAsProvider = this.navParams.get('openAsProvider');
        if (this.openAsProvider) {
            // this.useAddress = false;
        }
        this.init();
    }

    cancelRadius() {
        if (localStorage.getItem("DistanceRadius") != null) {
            this.distanceRadius = +localStorage.getItem("DistanceRadius");
            setTimeout(() => {
                this.radiusChanged = false;
            }, 100);

        } else {
            setTimeout(() => {
                this.radiusChanged = false;
            }, 100);
        };

    }

    updateRadius() {
        this.radiusChanged = true;

    }

    async goBack() {
        if (!this.openAsProvider) {
            if (this.addressSaved == null && !this.useGps) {
                var alert = await this.alertCtrl.create({
                    header: "No Location Saved",
                    message: "Please add your location to continue",
                    buttons: ['OK']
                });
                return await alert.present();
            }
            else {
                this.modalCtrl.dismiss();
            }
        }
        else {
            this.goBackAsProvider(false);
        }
    }

    goBackAsProvider(wasSuccessful) {
        this.modalCtrl.dismiss(wasSuccessful);
    }

    async saveRadius() {
        if ((this.addressSaved != null && this.addressSaved != undefined && this.addressSaved != "") || this.useGps) {
            this.radiusChanged = false;
            localStorage.setItem("DistanceRadius", this.distanceRadius.toString());
            // this.events.publish('RouteBroadcastRoot', 'TabsPage');
            // this.navCtrl.pop();
            this.navCtrl.navigateRoot('home');
            
        } else {
            var alert = await this.alertCtrl.create({
                header: "No Location Saved",
                message: "Please add your location to continue",
                buttons: ['OK']
            });
            return await alert.present();
        }

    }

    editAddress() {
        this.radiusChanged = false;
        this.addressSelected = false;
        this.showEdit = false;
    }

    cancelSearch() {
        this.addressSelected = false;
        this.address = "";
        this.term = "";
        this.addressPredictions = null;
    }

    clearSearch() {
        localStorage.setItem("DistanceRadius", this.distanceRadius.toString());
        this.addressSelected = false;
        this.address = "";
        this.term = "";
        this.addressPredictions = null;
        this.showButtons = false;
    }

    selectedAddress(address) {
        this.address = address;
        this.confirmAddress();
    }

    removeAddress(address) {
        if (this.userAddressBook.length == 1 && this.useAddress == true) {
            this.notificationService.presentAlertWithDescription("Address Not Removed", "At least one address must be selected");
        }
        else {
            if (localStorage.getItem("AddressBook") != undefined && localStorage.getItem("AddressBook") != "") {
                var addressbook = JSON.parse(localStorage.getItem("AddressBook"));
                addressbook = addressbook.filter(c => c != address);
                this.userAddressBook = addressbook;
                addressbook = JSON.stringify(addressbook);
                localStorage.setItem("AddressBook", addressbook);
                // this.userAddressBook = this.userAddressBook.filter(c => c != address);
                if (this.addressSaved == address) {
                    this.address = this.userAddressBook[0];
                    this.addressSaved = this.address;
                    // this.userAddressBook = this.userAddressBook.filter(c => c != address);
                    this.confirmAddress();
                }
            }

        }
    }

    clearAddressHistory() {
        this.userAddressBook = [];
        localStorage.setItem("AddressBook", "");
    }

    saveAddress() {
        if (localStorage.getItem("AddressBook") != undefined && localStorage.getItem("AddressBook") != "") {
            var _addressbook = JSON.parse(localStorage.getItem("AddressBook"));
            _addressbook.push(this.address);
            this.userAddressBook = _addressbook;
            _addressbook = JSON.stringify(_addressbook);
            localStorage.setItem("AddressBook", _addressbook);
        } else {
            var addressbook: any = [];
            addressbook.push(this.address);
            this.userAddressBook = addressbook;
            addressbook = JSON.stringify(addressbook);
            localStorage.setItem("AddressBook", addressbook);
        }
        this.confirmAddress();
    }

    async confirmAddress() {
        var loading = await this.loadingCtrl.create({
            message: 'Retrieving Location....'
        });

        await loading.present();

        this.userProvider.getLatLong(this.address)
            .then((response: any) => {
                localStorage.setItem('Latitude', response.Data.Latitude);
                localStorage.setItem('Longitude', response.Data.Longitude);
                this.cacheService.setCacheLocationPreference(Constants.LOCATION_PREFERENCE_ADDRESS);
                localStorage.setItem("AppConfigured", "True");
                localStorage.setItem("Address", this.address);

                this.addressSaved = this.address;
                this.address = "";
                this.addressSelected = true;
                this.showButtons = false;
                this.term = "";
                this.addressPredictions = null;
                this.showEdit = true;
                loading.dismiss();
                localStorage.setItem("DistanceRadius", this.distanceRadius.toString());
                this.toastService.presentToast("Viewing events from: " + this.addressSaved, true);
                // this.events.publish('RouteBroadcastRoot', 'TabsPage');
                // this.navCtrl.pop();
                this.navCtrl.navigateRoot('home');
            })
            .catch(err => {
                loading.dismiss();
                if (err.status == 0) {
                    var errMessage = {
                        Heading: "Internet connection lost",
                        Description: "Please reconnect to the internet and try again"
                    }
                    this.notificationService.showErrorMessage(errMessage);
                } else {
                    this.notificationService.showErrorMessage(err);
                }
                console.log(err);
            });
    }

    getPredictions(searchTerm) {
        this.loading = true;
        this.addressPredictions = [];
        this.radiusChanged = false;
        this.userProvider.getAddressPredictions(searchTerm)
            .then((response: any) => {
                this.loading = false;
                this.addressPredictions = response.Data;
                this.content.scrollToBottom();
            })
            .catch(err => {
                this.loading = false;
                console.log(err);
            });
    }

    chooseAddress(address) {
        this.address = address;
        this.addressSelected = true;
        this.term = address;
        this.showButtons = true;
        this.radiusChanged = false;
    }

    gpsToggle() {
        if (this.useGps) {
            this.useAddress = false;
            this.useGps = true;

            if (this.platform.is('ios')) {
                this.gpsFirstRun(false);
            }
            else {
                this.locationAccuracy.canRequest().then((canRequest: boolean) => {

                    if (canRequest) {
                        // the accuracy option will be ignored by iOS
                        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                            () => {
                                this.gpsFirstRun(false);
                            },
                            error => {
                                this.cacheService.setCacheLocationPreference("");
                                this.useGps = false;
                                this.useAddress = true;
                                console.log('Error requesting location permissions', error);
                            }
                        );
                    } else {
                        this.gpsFirstRun(false);
                    }
                });
            }
        }
        else {
            this.useAddress = true;
            this.useGps = false;
        }
    }

    addressToggle() {
        if (!this.openAsProvider) {
            if (this.useAddress) {
                this.useGps = false;
                this.useAddress = true;
            }
            else {
                this.useGps = true;
                this.useAddress = false;
            }
        }
        else {
            this.useGps = false;
            this.useAddress = false;
            this.notificationService.presentAlert("You cannot use address, GPS is required when checking in.");
        }
    }

    clearAddress() {
        this.address = "";
        this.addressSelected = false;
        this.term = "";
        this.addressPredictions = null;
    }

    // closeKeyboard() {
    //     this.keyboard.hide();
    // }

    gpsFirstRun(refresh) {
        this.platform.ready().then(() => {
            this.locationService.isLocationAuthorized().then(
                response => {
                    if (response) {
                        this.locationService.getGpsAvailability().then(response => {
                            if (response) {
                                console.log("GPS Available");
                                this.locationService.getUserLocation().then(response => {
                                    console.log("Location Retrieved");
                                    localStorage.setItem("DistanceRadius", this.distanceRadius.toString());
                                    this.toastService.presentToast("We have successfully located your device", true);
                                    if (!this.openAsProvider) {
                                        if (refresh) {
                                            // this.events.publish('RouteBroadcastRoot', 'locationRefreshed');
                                        } else {
                                            // this.events.publish('RouteBroadcastRoot', 'TabsPage');
                                            this.navCtrl.navigateRoot("/home");
                                        }
                                    }
                                    else {
                                        this.goBackAsProvider(true);
                                    }
                                },
                                    error => {
                                        console.log("Location Not Retrieved");
                                        // this.events.publish('initFailed');
                                        this.showGpsErrorAlert();
                                    });
                            }
                            else {
                                console.log("GPS Not Available");
                                // this.events.publish('initFailed');
                                this.showEnableLocationAlert();
                            }
                        },
                            error => {
                                console.log("GPS Not Available Error");
                                this.cacheService.setCacheLocationPreference("");
                                // this.events.publish('initFailed');
                                this.useGps = false;
                                this.useAddress = true;
                            });
                    }
                    else {
                        this.locationService.requestLocationAuthorization().then(response => {
                            if (response) {
                                console.log("Location Authorized");
                                this.locationService.getGpsAvailability().then(response => {
                                    if (response) {
                                        console.log("GPS Available");

                                        this.locationService.getUserLocation().then(response => {
                                            console.log("Location Retrieved");
                                            localStorage.setItem("DistanceRadius", this.distanceRadius.toString());
                                            this.toastService.presentToast("We have successfully located your device", true);
                                            if (!this.openAsProvider) {
                                                // this.events.publish('RouteBroadcastRoot', 'TabsPage');
                                                this.navCtrl.navigateRoot("/home");
                                            }
                                            else {
                                                this.goBackAsProvider(true);
                                            }
                                        },
                                            error => {
                                                console.log("Location Not Retrieved");
                                                // this.events.publish('initFailed');
                                                this.showGpsErrorAlert();
                                            });
                                    }
                                    else {
                                        console.log("GPS Not Available");
                                        // this.events.publish('initFailed');
                                        this.showEnableLocationAlert();
                                    }
                                },
                                    error => {
                                        console.log("GPS Not Available Error");
                                        this.cacheService.setCacheLocationPreference("");
                                        // this.events.publish('initFailed');
                                        this.useGps = false;
                                        this.useAddress = true;
                                    });
                            }
                            else {
                                console.log("Location Not Authorized");
                                this.showGpsUnauthorizedAlert();
                            }

                        },
                            error => {
                                this.locationService.isDeviceLocationEnabled().then(response => {
                                    console.log("Device Location Enabled");
                                    this.cacheService.setCacheLocationPreference(Constants.LOCATION_PREFERENCE_ADDRESS);
                                    this.useGps = false;
                                    this.useAddress = true;
                                    // this.events.publish('RouteBroadcastRoot', 'LocationModal');
                                },
                                    error => {
                                        console.log("Device Location Not Enabled Error");
                                        this.cacheService.setCacheLocationPreference("");
                                        // this.events.publish('initFailed');
                                        this.useGps = false;
                                        this.useAddress = true;
                                    });
                            });
                    }
                });
        })
    }

    async showGpsUnavailableAlert() {
        var alert = await this.alertCtrl.create({
            header: "GPS Not Available",
            message: "Please Try Again",
            buttons: ['OK']
        });
        this.cacheService.setCacheLocationPreference("");
        this.useAddress = false;
        this.useGps = false;

        return await alert.present();
    }

    async showGpsUnauthorizedAlert() {
        var alert = await this.alertCtrl.create({
            header: "GPS Permission Not Granted",
            message: "Please Try Again",
            buttons: ['OK']
        });
        this.cacheService.setCacheLocationPreference("");
        this.useAddress = false;
        this.useGps = false;

        return await alert.present();
    }

    async showGpsErrorAlert() {
        var alert = await this.alertCtrl.create({
            header: "Unable to find gps coordinates",
            message: "Please Try Again",
            buttons: ['OK']
        });
        this.cacheService.setCacheLocationPreference("");
        this.useGps = false;

        return await alert.present();
    }

    async showEnableLocationAlert() {
        var alert = await this.alertCtrl.create({
            header: "GPS not enabled",
            message: "Please enable your device's GPS.",
            buttons: [{
                text: 'Location Settings',
                role: 'cancel',
                handler: () => {
                    this.diagnostic.switchToLocationSettings();
                    setTimeout(() => {
                        this.gpsFirstRun(false);
                    }, 2000);
                }
            },
            {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                    this.cacheService.setCacheLocationPreference("");
                    this.useGps = false;
                }
            }]
        });
        
        return await alert.present();
    }

}