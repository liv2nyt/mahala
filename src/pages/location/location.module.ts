import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LocationPage } from './location.page';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: "",
    component: LocationPage
  }
];

@NgModule({
  imports: [
    IonicModule,
    FormsModule,
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LocationPage]
})
export class LocationPageModule {}
