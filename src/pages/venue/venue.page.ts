import { Component, ViewChild, OnInit } from "@angular/core";
import {
    // Events,
    LoadingController,
    NavParams,
    AlertController,
    ModalController,
    NavController,
    Platform
} from "@ionic/angular";
import { ApiService } from "../../services/api-service";
import {
    LaunchNavigator,
    LaunchNavigatorOptions
} from "@ionic-native/launch-navigator/ngx";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
import {
    VenueType,
    Hours,
    ServiceResult,
    VenueResult,
    TrophyRoom,
    LeaderBoardType,
    Level,
    BadgeType,
    UserBadge,
    UserReward,
    LoginResult,
    ClaimUserRewardRequest,
    Reward
} from "../../contracts/contracts";
import { CallNumber } from "@ionic-native/call-number/ngx";
// import { PhotoViewer } from "@ionic-native/photo-viewer";
import { NotificationService } from "../../services/notification-service";
import { ToastService } from "../../services/toast-service";
import { CacheService } from "../../services/cache-service";
import { ActivatedRoute } from '@angular/router';
import { ClaimRewardPage } from '../claim-reward/claim-reward.page';
import { RecommendationsPage } from '../recommendations/recommendations.page';
import { LocationService } from 'src/services/location-service';
import { LocationPage } from '../location/location.page';
import { VerifyBillPage } from '../verify-bill/verify-bill.page';

@Component({
    selector: "venue",
    templateUrl: "./venue.page.html",
    styleUrls: ["./venue.page.scss"]
})
export class VenuePage implements OnInit {
    @ViewChild("content", null) content: any;
    eventImages: any = [];
    spotImages: any = [];
    openShare: boolean = false;
    venueId: number = 0;
    venueData: VenueResult;
    venueImages: any = {
        Image: null,
        TinyImage: null
    };
    start: string;
    destination: string;
    checkedIn: boolean = false;
    venueTypes: VenueType[] = [];
    hours: Hours = new Hours();
    weekday = [];
    shareMessageData: string = "";
    hoursAvailable: boolean = false;
    showHours: boolean = false;
    hardBackPress: boolean = false;
    following: boolean = false;
    isModal: boolean = false;
    loading: boolean = false;
    navOptions: string[] = [];
    imageLoaded: boolean = false;
    friendsWhoVisitedVenue: any = [];
    friendsWhoVisitedVenueExpanded: boolean = false;
    friendsWhoVisitedVenueNoToShow: number = 4;
    order: any[] = [];
    selectedOption: string = "info";
    trophyRoomLoaded: boolean = false;
    trophyRoomLoading: boolean = false;
    trophyRoomData: TrophyRoom = null;
    nextLevel: string = "Fan";

    points: number = 50;

    leaderSelectedOption: string = "global";

    newOrder: boolean = false;
    reward1claimed: boolean = false;
    reward2claimed: boolean = false;
    leaderBoardTypes: LeaderBoardType[] = [];
    globalLeaderBoardType: number = 0;
    friendsLeaderBoardType: number = 1;

    progressWidth: number = 1;
    badgeTypes: BadgeType[];
    checkInBadges: UserBadge[];
    inviteBadges: UserBadge[];
    recommendationBadges: UserBadge[];
    user: LoginResult;

    constructor(
        private callNumber: CallNumber,
        // private photoViewer: PhotoViewer,
        private platform: Platform,
        private socialSharing: SocialSharing,
        private iab: InAppBrowser,
        private launchNav: LaunchNavigator,
        private navParams: NavParams,
        private userProvider: ApiService,
        private loadingCtrl: LoadingController,
        // private events: Events,
        private alertCtrl: AlertController,
        private modalCtrl: ModalController,
        private notificationService: NotificationService,
        private toastService: ToastService,
        private cacheService: CacheService,
        private navCtrl: NavController,
        private aR: ActivatedRoute,
        private locationService: LocationService
    ) {
    }

    ngOnInit() {
        this.aR.queryParams.subscribe(params => {
            if (params && params.venueId) {
              this.venueId = params.venueId;
            }
            if (params.isModal) {
                this.isModal = params.isModal;
            }
            if(params.venueImages)
            {
                this.venueImages = params.venueImages;
            }
        });
        this.initialize();
        this.setWeekDay();
        this.getVenue();
        this.loadTrophyRoom();
        this.initNavOptions();
    }

    initialize() {
        this.venueTypes = this.userProvider.initializeVenueTypes();
        this.leaderBoardTypes = this.cacheService.getCachedLeaderBoardTypes();
        // this.globalLeaderBoardType = this.leaderBoardTypes.filter(c => c.Description == "Global")[0].Id - 1;
        // this.friendsLeaderBoardType = this.leaderBoardTypes.filter(c => c.Description == "Friends")[0].Id - 1;
        this.badgeTypes = this.cacheService.getCachedBadgeTypes();
        this.user = this.cacheService.getCachedUser();

        var image = this.venueImages.Image;

        if (image != undefined && image != null && image != "") {
            this.imageLoaded = true;
        }
    }

    expandFriendsWhoVisitedVenue() {
        this.friendsWhoVisitedVenueExpanded = !this
            .friendsWhoVisitedVenueExpanded;
    }

    getVenue() {
        this.loading = true;
        var spotDistance = {
            VenueId: this.venueId,
            Latitude: 0,
            Longitude: 0
        };
        spotDistance.Latitude = +localStorage.getItem("Latitude");
        spotDistance.Longitude = +localStorage.getItem("Longitude");
        this.userProvider
            .loadSpotCall(spotDistance)
            .then((response: ServiceResult<VenueResult>) => {
                this.loading = false;
                if (response.WasSuccessful) {
                    this.venueData = response.Data;
                    if (!this.imageLoaded) {
                        if (
                            response.Data.Image != undefined &&
                            response.Data.Image != null &&
                            response.Data.Image != ""
                        ) {
                            this.venueImages.TinyImage =
                                response.Data.TinyImage +
                                "?" +
                                new Date().getTime();
                            this.venueImages.Image =
                                response.Data.Image +
                                "?" +
                                new Date().getTime();
                        } else {
                            this.venueImages.Image =
                                "assets/imgs/noImageMedium.png";
                            this.venueImages.TinyImage =
                                "assets/imgs/noImageMedium.png";
                        }
                    }
                    this.checkedIn = this.venueData.CheckedIn;
                    this.following = this.venueData.Following;
                    this.hours = response.Data.Hours;
                    this.hoursAvailable = response.Data.IsHoursAvailable;
                    this.friendsWhoVisitedVenue =
                        response.Data.FriendsWhoVisitedVenue;
                    this.rootTo();
                } else {
                    this.notificationService.showErrorMessage(response);
                }
            })
            .catch(err => {
                if (err.status == 0) {
                    var errMessage = {
                        Heading: "Internet connection lost",
                        Description:
                            "Please reconnect to the internet and try again"
                    };
                    this.notificationService.showErrorMessage(errMessage);
                    this.goBack();
                } else {
                    this.notificationService.showErrorMessage(err);
                    this.loading = false;
                }
            });
    }

    rootTo() {
        var rootTo = localStorage.getItem("rootTo");
        if (rootTo == "VenueRecommendationReply") {
            localStorage.setItem("rootTo", "");
            localStorage.setItem("VenueRecommendationReply", "0");
            // this.presentRecommendationsModal();
        }
    }

    initNavOptions() {
        var google = "Google Maps";
        var apple = "Apple Maps";
        var waze = "Waze";
        this.navOptions.push(google);
        this.navOptions.push(apple);
        this.navOptions.push(waze);
    }

    swipe(event) {
        if (event.direction === 4) {
            this.goBack();
        }
    }

    callNumberActivate(number) {
        this.callNumber
            .callNumber(number, true)
            .then(res => console.log("Launched dialer!", res))
            .catch(err => console.log("Error launching dialer", err));
    }

    async callNumberAlert(number) {
        const confirm = await this.alertCtrl.create({
            message: "Would you like place a call to '" + number + "'?",
            buttons: [
                {
                    text: "No"
                },
                {
                    text: "Yes",
                    handler: () => {
                        this.callNumberActivate(number);
                    }
                }
            ]
        });
        
        return await confirm.present();
    }

    goBack() {
        if (this.isModal) {
            this.dismiss();
        } else {
            this.navCtrl.navigateBack('home');
            
            if (this.navParams.get("deeplink") == true) {
                // this.viewCtrl.dismiss();
                this.navCtrl.navigateBack('home');
            }
        }
    }

    dismiss() {
        // this.viewCtrl.dismiss(false);
        // this.navCtrl.pop();
        this.modalCtrl.dismiss();
    }

    shareClick() {
        if (this.openShare == false) {
            this.openShare = true;
        } else {
            this.openShare = false;
        }
        setTimeout(() => {
            this.content.scrollToBottom(200);
        }, 300);
    }

    openWebsite(url) {
        var browser = this.iab.create(url, "_self");
        browser.show();
    }

    openNavigation(navOption) {
        var openUrl = "";
        var app = null;
        var latitude = +localStorage.getItem("Latitude");
        var longitude = +localStorage.getItem("Longitude");
        var origin = latitude + "," + longitude;
        this.destination =
            this.venueData.Location.Latitude +
            "," +
            this.venueData.Location.Longitude;

        switch (navOption) {
            case "Google Maps":
                openUrl =
                    "https://www.google.com/maps/dir/?api=1&origin=" +
                    origin +
                    "&destination=" +
                    this.destination +
                    "&travelmode=driving";
                app = this.launchNav.APP.GOOGLE_MAPS;
                break;
            case "uber":
                openUrl = "https://www.uber.com";
                app = this.launchNav.APP.UBER;
                break;
            case "Waze":
                openUrl = "https://www.waze.com";
                app = this.launchNav.APP.WAZE;
                break;
            case "Apple Maps":
                openUrl = "https://mapsconnect.apple.com/";
                app = this.launchNav.APP.APPLE_MAPS;
                break;
        }

        this.launchNav
            .isAppAvailable(app)
            .then(success => {
                if (!success) {
                    const browser = this.iab.create(openUrl);
                    browser.show();
                }
                this.start = "";
                let options: LaunchNavigatorOptions = {
                    start: this.start,
                    app: app
                };
                this.launchNav.navigate(this.destination, options).then(
                    () => {},
                    error => {
                        console.log(error);
                    }
                );
            })
            .catch(error => {
                console.log(error);
                const browser = this.iab.create(openUrl);
                browser.show();
            });
    }

    async showNavOptionInputAlert() {
        var isIOS = false;
        if (this.platform.is("ios")) {
            isIOS = true;
        }
        if (isIOS) {
            let alert = await this.alertCtrl.create({
                header: "Navigate with:",
                buttons: [
                    {
                        text: "Google Maps",
                        handler: () => {
                            this.openNavigation("Google Maps");
                        }
                    },
                    {
                        text: "Apple Maps",
                        handler: () => {
                            this.openNavigation("Apple Maps");
                        }
                    },
                    {
                        text: "Waze",
                        handler: () => {
                            this.openNavigation("Waze");
                        }
                    },
                    {
                        text: "Cancel"
                    }
                ]
            });

            return await alert.present();
        } else {
            let alert = await this.alertCtrl.create({
                header: "Navigate with:",
                buttons: [
                    {
                        text: "Google Maps",
                        handler: () => {
                            this.openNavigation("Google Maps");
                        }
                    },
                    {
                        text: "Waze",
                        handler: () => {
                            this.openNavigation("Waze");
                        }
                    },
                    {
                        text: "Cancel"
                    }
                ]
            });
            
            return await alert.present();
        }
    }

    async shareMessage(type) {
        const prompt = await this.alertCtrl.create({
            message: "Message for readers",
            inputs: [
                {
                    placeholder: "Message",
                    value: this.shareMessageData,
                    name: "shareMessage"
                }
            ],
            buttons: [
                {
                    text: "Skip",
                    handler: () => {
                        if (type == "1") {
                            this.shareWhatsApp();
                        }
                    }
                },
                {
                    text: "Add",
                    handler: data => {
                        this.shareMessageData = data.shareMessage;
                        if (type == "1") {
                            this.shareWhatsApp();
                        }
                    }
                }
            ]
        });
        prompt.present();
    }

    async shareWhatsApp() {
        let loading = await this.loadingCtrl.create({
            message: "Loading Image"
        });

        await loading.present();
        var url = "http://mahala.com/venue?venueId=" + this.venueData.Id;

        this.socialSharing
            .shareViaWhatsApp(
                this.venueData.Name + "\n\n" + this.shareMessageData,
                this.venueImages.Image,
                "\n \nFor more information about the above restaurant, " +
                " visit or download the Mahala app from the app stores. \n \n " +
                    url +
                    "\n \n " +
                    "www.ambitiondev.com"
            )
            .then(() => {
                return loading.dismiss();
            })
            .catch(err => {
                console.log(err);
                const browser = this.iab.create("https://www.whatsapp.com/");
                browser.show();
                return loading.dismiss();
            });
    }

    confirmLocation() {
        this.locationService.confirmLocation(this.venueData.Location.Latitude, this.venueData.Location.Longitude)
        .then((wasSuccessful) => {
            if (wasSuccessful) {
                this.checkIn();
            }
            else {
                this.notificationService.presentAlertWithDescription(
                    "It seems like you are quite far away from " + this.venueData.Name,
                    "Please try again");
            }
        })
        .catch(() => {
            this.showGpsLocationErrorMessage();
            // this.getTables(); for testing
        });
    }

    async showGpsLocationErrorMessage() {
        var alert = await this.alertCtrl.create({
            header: 'We are unable to find our location',
            message: 'Please select GPS on the Location Page',
            buttons: [{
                text: "Ok",
                handler: () => {
                    this.openLocationModal();
                }
            }],
        });
        
        return await alert.present();
    }

    async openLocationModal() {
        var locationModal = await this.modalCtrl.create({
            component: LocationPage, 
            componentProps: { openAsProvider: true }
        });
        locationModal.onDidDismiss().then(((wasSuccessful) => {
            if (wasSuccessful) {
                this.confirmLocation();
            }
            else {
                this.notificationService.presentAlertWithDescription("We are still unable to find our location", "Please try again & select GPS");
            }
        }));

        return await locationModal.present();
    }


    async checkIn() {
        let loading = await this.loadingCtrl.create({
            message: "Checking In..."
        });

        await loading.present();

        var checkInRequest = {
            VenueId: this.venueData.Id,
            Latitude: 0,
            Longitude: 0
        };

        this.userProvider
            .checkIn(checkInRequest)
            .then((response: any) => {
                if (response.WasSuccessful) {
                    this.checkedIn = true;
                    this.toastService.presentToast(
                        "You have successfully checked in",
                        true
                    );
                    // this.events.publish("userCheckIn");
                } else {
                    this.notificationService.showErrorMessage(response);
                }
                return loading.dismiss();
            })
            .catch(err => {
                if (err.status == 0) {
                    var errMessage = {
                        Heading: "Internet connection lost",
                        Description:
                            "Please reconnect to the internet and try again"
                    };
                    this.notificationService.showErrorMessage(errMessage);
                } else {
                    this.notificationService.showErrorMessage(err);
                }
                return loading.dismiss();
            });
    }

    async checkOutConfirmation() {
        const confirm = await this.alertCtrl.create({
            message: "Are you sure you would like to cancel check in?",
            buttons: [
                {
                    text: "No"
                },
                {
                    text: "Yes",
                    handler: () => {
                        this.checkOut();
                    }
                }
            ]
        });
        
        return await confirm.present();
    }

    async checkOut() {
        let loading = await this.loadingCtrl.create({
            message: "Cancelling checking in..."
        });

        await loading.present();

        this.userProvider
            .checkOut()
            .then((response: any) => {
                loading.dismiss();
                if (response.WasSuccessful) {
                    this.checkedIn = false;
                    this.toastService.presentToast(
                        "You have successfully checked out",
                        true
                    );
                } else {
                    this.notificationService.showErrorMessage(response);
                }
            })
            .catch(err => {
                loading.dismiss();
                if (err.status == 0) {
                    var errMessage = {
                        Heading: "Internet connection lost",
                        Description:
                            "Please reconnect to the internet and try again"
                    };
                    this.notificationService.showErrorMessage(errMessage);
                } else {
                    this.notificationService.showErrorMessage(err);
                }
            });
    }

    operatingHours() {
        this.showHours = !this.showHours;
    }

    setWeekDay() {
        this.weekday[0] = "Sunday";
        this.weekday[1] = "Monday";
        this.weekday[2] = "Tuesday";
        this.weekday[3] = "Wednesday";
        this.weekday[4] = "Thursday";
        this.weekday[5] = "Friday";
        this.weekday[6] = "Saturday";
    }

    async presentRecommendationsModal() {
        let recommendationsModal = await this.modalCtrl.create({
            component: RecommendationsPage,
            componentProps:
            {
                isVenueOwner: false,
                venueName: this.venueData.Name,
                venueId: this.venueId,
                recommended: this.venueData.RecommendedCount,
                notRecommended: this.venueData.NotRecommendedCount
            }
        });

        recommendationsModal.onDidDismiss().then((response => {
            if (response.data.didAddNewRecommendation) {
                if (response.data.addedRecommended) {
                    this.venueData.RecommendedCount++;
                } else {
                    this.venueData.NotRecommendedCount++;
                }
            }
        }));

        return await recommendationsModal.present();
    }

    // presentProfileModal(userId) {
    //     let profileModal = this.modalCtrl.create("FriendsProfilePage", {
    //         userId: userId
    //     });
    //     profileModal.present();
    // }

    trophyRoomClicked() {
        if (!this.trophyRoomLoaded) {
            if (this.selectedOption == "trophyRoom") {
                this.trophyRoomLoaded = true;
                // this.loadTrophyRoom(); to do when connected to backend functionality
            }
        }
    }

    loadTrophyRoom() {
        this.trophyRoomLoaded = true;
        this.userProvider
            .getTrophyRoom(this.venueId)
            .then((response: ServiceResult<TrophyRoom>) => {
                this.trophyRoomLoading = false;
                if (response.WasSuccessful) {
                    this.trophyRoomData = response.Data;
                    this.calculateProgressWidth(
                        this.trophyRoomData.CurrentLevel,
                        this.trophyRoomData.NextLevel,
                        this.trophyRoomData.Points
                    );
                    this.mapUserBadges(this.trophyRoomData.UserBadges);
                } else {
                    this.trophyRoomData = null;
                }
            })
            .catch(err => {
                this.trophyRoomLoading = false;
                if (err.status == 0) {
                    this.trophyRoomData = null;
                    this.notificationService.presentNoInternetAlert();
                } else {
                    this.trophyRoomData = null;
                }
            });
    }

    leaderGlobalClicked() {}

    calculateProgressWidth(
        currentLevel: Level,
        nextLevel: Level,
        userPoints: number
    ) {
        if (userPoints != 0) {
            var high = nextLevel.MinPoints - currentLevel.MinPoints;
            var low = userPoints - currentLevel.MinPoints;
            this.progressWidth = (low / high) * 100;
        }
    }

    mapUserBadges(userBadges: any) {
        this.checkInBadges = userBadges.filter(c => c.Badge.BadgeTypeId == 1);
        this.inviteBadges = userBadges.filter(c => c.Badge.BadgeTypeId == 2);
        this.recommendationBadges = userBadges.filter(
            c => c.Badge.BadgeTypeId == 3
        );
    }

    async claimReward(reward: Reward) {
        let loading = await this.loadingCtrl.create({
            message: "Sending reward claim request"
        });

        await loading.present();

        var req: ClaimUserRewardRequest = {
            VenueId: this.venueId,
            RewardId: reward.Id
        };

        this.userProvider
            .claimReward(req)
            .then((response: ServiceResult<UserReward>) => {
                if (response.WasSuccessful) {
                    this.qrCode(response.Data);
                    return loading.dismiss();
                } else {
                    this.notificationService.presentAlertWithDescription(
                        response.Heading,
                        response.Description
                    );
                    return loading.dismiss();
                }
            })
            .catch(err => {
                if (err.status == 0) {
                    this.notificationService.presentNoInternetAlert();
                    return loading.dismiss();
                } else {
                    this.notificationService.presentAlertWithDescription(
                        "Reward claim request failed",
                        "Unknown error"
                    );
                    return loading.dismiss();
                }
            });
    }

    async qrCode(userReward: UserReward) {
        if (!userReward.Claimed) {
            let qrCodeModal = await this.modalCtrl.create({
                component: ClaimRewardPage,
                componentProps: {
                    userReward : userReward
                }
            });
            qrCodeModal.onDidDismiss().then((response => {
                if (response.data) {
                    this.trophyRoomData.Points -=
                        userReward.Reward.PointsNeeded;
                }
            }));
            return await qrCodeModal.present();
        }
    }

    async verifyBill() {
        let verifyBillModal = await this.modalCtrl.create({
            component: VerifyBillPage,
            componentProps: {
                userId : this.user.UserId,
                venueId: this.venueId
            }
        });

        verifyBillModal.onDidDismiss().then((response => {
            if (response.data) {
                this.trophyRoomData.Points +=
                response.data;
            }
        }));

        return await verifyBillModal.present();
    }
}
