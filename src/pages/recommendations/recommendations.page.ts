import { Component, NgZone, OnInit } from "@angular/core";
import { ModalController, NavParams, AlertController } from "@ionic/angular";
import { ApiService } from "../../services/api-service";
import { NotificationService } from "../../services/notification-service";
import { CacheService } from "../../services/cache-service";
import { RecommendationResult, ImageResult } from "../../contracts/contracts";
import { ImageService } from "../../services/image-service";
import { Constants } from "../../contracts/constants";
import { AddRecommendationPage } from './add-recommendation/add-recommendation.page';
// import { PhotoViewer } from "@ionic-native/photo-viewer";

@Component({
    templateUrl: './recommendations.page.html',
    selector: 'recommendations',
    styleUrls: ['./recommendations.page.scss']
})
export class RecommendationsPage implements OnInit {
    loading: any = false;
    venueId = 0;
    venueName: string = '';
    recommendations: RecommendationResult[] = [];
    recommended: number = 0;
    notRecommended: number = 0;
    page = 1;
    showInfiniteScroll: boolean = false;
    userId: any = 0;
    dismissData = {
        didAddNewRecommendation: false,
        addedRecommended: false
    };

    constructor(private userProvider: ApiService,
        private modalCtrl: ModalController,
        private alertCtrl: AlertController,
        private navParams: NavParams,
        private notificationService: NotificationService,
        private cacheService: CacheService,
        private imageService: ImageService,
        private zone: NgZone,
        // private photoViewer: PhotoViewer
        ) {
    }

    ngOnInit() {
        // this.venueId = this.navParams.get("venueId");
        // this.recommended = this.navParams.get("recommended");
        // this.notRecommended = this.navParams.get("notRecommended");
        // this.venueName = this.navParams.get("venueName");
        this.userId = this.cacheService.getUserId();
        this.loadVenueRecommendations();
    }

    // async presentProfileModal(userId) {
    //     if (this.userId != userId) {
    //         let profileModal = await this.modalCtrl.create({
    //             component: ProfilePage, 
    //             componentProps: userId
    //         });
    //         return await profileModal.present();
    //     }
    // }

    goBack() {
        this.modalCtrl.dismiss(this.dismissData);
    }

    loadVenueRecommendations() {
        this.recommendations = [];
        this.loading = true;
        this.showInfiniteScroll = true;

        this.userProvider.getVenueRecommendations(this.venueId, this.page)
            .then((response: any) => {
                this.loading = false;
                if (response.WasSuccessful) {
                    this.recommendations = response.Data;
                    if (response.Data.length == 0) {
                        this.showInfiniteScroll = false;
                    }
                }
            })
            .catch(err => {
                this.loading = false;
                this.recommendations = [];
                if (err.status == 0) {
                    this.notificationService.presentNoInternetAlert();
                    this.goBack();
                }
                console.log(err);
            });
    }

    doInfinite(infiniteScroll) {
        this.page++;
        this.doInfiniteVenue(infiniteScroll);
    }

    doInfiniteVenue(event) {
        var infiniteScroll = event.target;

        this.userProvider.getVenueRecommendations(this.venueId, this.page)
            .then((response: any) => {
                infiniteScroll.complete();
                if (response.WasSuccessful) {
                    if (response.Data.length < 20) {
                        this.showInfiniteScroll = false;
                    }

                    if (response.Data.length == 0) {
                        this.showInfiniteScroll = false;
                    }

                    this.recommendations.push(...response.Data);
                }
            })
            .catch(err => {
                this.showInfiniteScroll = false;
                infiniteScroll.complete();
                if (err.status == 0) {
                    this.notificationService.presentNoInternetAlert();
                }
                console.log(err);
            });
    }

    async addRecommendation() {
        var venueId = this.venueId;

        let addRecommendationModal = await this.modalCtrl.create({
            component: AddRecommendationPage, 
            componentProps: { venueName: this.venueName, venueId: venueId }
        });

        addRecommendationModal.onDidDismiss().then((response => {
            if (response.data) {
                var recomendationResult: RecommendationResult = response.data;
                this.recommendations.unshift(recomendationResult);
                this.dismissData.didAddNewRecommendation = true;
                if (response.data.Recommended) {
                    this.dismissData.addedRecommended = true;
                    this.recommended++;
                }
                else {
                    this.dismissData.addedRecommended = false;
                    this.notRecommended++;
                }
            }
        }));
        return await addRecommendationModal.present();
    }

    uploadImage(recommendationId) {
        this.imageService.uploadImage(this.userProvider.url + "/Image/Recommendation/" + recommendationId, "Recommendation")
        .then((response: ImageResult) => {
            this.zone.run(() => {
                this.recommendations.filter(c => c.Id == recommendationId)[0].ListImage = response.ListImage + "?" + new Date().getTime();
            });
        }).catch(error => {
            if(error != "Image not selected")
            {
                this.notificationService.presentAlertWithDescription('Failed to Upload Recommendation Image', "The image failed to upload, please ensure you have an internet connection and try again");
            }
            console.log(error);
        });;
    }

    openImage(image) {
        // this.photoViewer.show(image,'', Constants.PHOTO_VIEWER_OPTIONS);
    }
}