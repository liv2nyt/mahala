import { Component } from "@angular/core";
import {
  NavParams,
  AlertController,
  LoadingController,
  ModalController
} from "@ionic/angular";
import { ApiService } from "../../../services/api-service";
import { NotificationService } from "../../../services/notification-service";
import { CacheService } from "../../../services/cache-service";
import {
  RecommendationResult,
  ServiceResult
} from "../../../contracts/contracts";

@Component({
  templateUrl: "./add-recommendation.page.html",
  selector: "add-recommendation",
  styleUrls: ['./add-recommendation.page.scss']
})
export class AddRecommendationPage {
  venueId = 0;
  venueName: string = "";
  page = 1;
  showInfiniteScroll: boolean = false;
  userId: any = 0;
  review: string = "";
  recommendation: RecommendationResult;
  recommended: boolean = true;
  notRecommended: boolean = false;
  publicPost: boolean = true;

  constructor(
    private alertCtrl: AlertController,
    private userProvider: ApiService,
    private modalCtrl: ModalController,
    private navParams: NavParams,
    private notificationService: NotificationService,
    private loadingCtrl: LoadingController,
    private cacheService: CacheService
  ) {}

  ionViewDidLoad() {
    this.venueId = this.navParams.get("venueId");
    this.venueName = this.navParams.get("venueName");
    this.userId = this.cacheService.getUserId();
  }

  goBack() {
    this.modalCtrl.dismiss(this.recommendation);
  }

  async addRecommendation() {
    let loading = await this.loadingCtrl.create({
      message: "Adding..."
    });

    await loading.present();

    var req = {
      VenueId: this.venueId,
      Review: this.review,
      Recommended: this.recommended ? true : false,
      Public: this.publicPost
    };

    this.userProvider
      .addRecommendation(req)
      .then((response: ServiceResult<RecommendationResult>) => {
        // loading.dismiss();
        if (response.WasSuccessful) {
          this.recommendation = response.Data;
          this.showAlert(response.Heading);
        }
        return loading.dismiss();
      })
      .catch(err => {
        // loading.dismiss();
        if (err.status == 0) {
          this.notificationService.presentNoInternetAlert();
        } else {
          this.notificationService.showErrorMessage(err);
        }
        console.log(err);
        return loading.dismiss();
      });
  }

  async showAlert(message) {
    const prompt = await this.alertCtrl.create({
      message: message,
      buttons: [
        {
          text: "Ok",
          handler: () => {
            this.goBack();
          }
        }
      ]
    });

    return await prompt.present();
  }

  recommendedClick() {
    this.recommended = true;
    this.notRecommended = false;
  }

  notRecommendedClick() {
    this.recommended = false;
    this.notRecommended = true;
  }

  async showPublicRadio() {

    var inputs = [];

    // alert.setTitle("Share with");

    if (this.publicPost) {
        inputs =  [
                {
                    type: "radio",
                    label: "Public",
                    value: "public",
                    checked: true
                },
                {
                    type: "radio",
                    label: "Only Friends",
                    value: "notPublic",
                    checked: false
                }
              ];

    //   alert.addInput({
    //     type: "radio",
    //     label: "Public",
    //     value: "public",
    //     checked: true
    //   });

    //   alert.addInput({
    //     type: "radio",
    //     label: "Only Friends",
    //     value: "notPublic",
    //     checked: false
    //   });
    } else {
        inputs = [
                {
                    type: "radio",
                    label: "Public",
                    value: "public",
                    checked: false
                },
                {
                    type: "radio",
                    label: "Only Friends",
                    value: "notPublic",
                    checked: true
                }
              ];
    }

    let alert = await this.alertCtrl.create({
        inputs: inputs,
        header: 'Share with',
        buttons: [
            'Cancel',
            {
                text: "Ok",
                handler: data => {
                  if (data == "public") {
                    this.publicPost = true;
                  } else {
                    this.publicPost = false;
                  }
                }
              }
        ]
      });

    // alert.addButton("Cancel");
    // alert.addButton({
    //   text: "Ok",
    //   handler: data => {
    //     if (data == "public") {
    //       this.publicPost = true;
    //     } else {
    //       this.publicPost = false;
    //     }
    //   }
    // });
    
    return await alert.present();
  }
}
