import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, AlertController, ToastController, ModalController, IonContent } from '@ionic/angular';
import { IdentityService } from '../../services/identity-service';
import { VenueType, SpotDistanceFilter, AddToBill, ServiceResult, UserNotificationResult } from '../../contracts/contracts';
import { ApiService } from '../../services/api-service';
// import { Platform } from 'ionic-angular/platform/platform';
// import { AppMinimize } from '@ionic-native/app-minimize';
// import { SpotPage } from '../spot/spot';
import { LocationService } from '../../services/location-service';
import { NotificationService } from '../../services/notification-service';
import { CacheService } from '../../services/cache-service';
import { Constants } from '../../contracts/constants';
import { Platform } from '@ionic/angular';
import { NavigationOptions } from '@ionic/angular/providers/nav-controller';
import { SearchVenuePage } from '../search/search-venue/search-venue.page';
import { ProfilePage } from '../profile/profile.page';

@Component({
    selector: 'home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss']
})
export class HomePage {
    // @ViewChild(Refresher, null) refresher: Refresher;
    @ViewChild('pageTop', null) pageTop: IonContent;
    // @ViewChild('fab', null) fab: FabContainer;
    distanceRadius = 100;
    showLoading: boolean = false;
    venues: any[] = [];
    venuesBackup: any[] = [];
    pageNumber: number = 1;
    noInternet: boolean = false;
    showInfiniteScroll: boolean = true;
    venueTypes: VenueType[] = [];
    showHours: boolean = false;
    date = new Date();
    loaded: boolean = true;
    showEndOfList: boolean = false;
    dayOfWeek: number = 0;
    // filterAlert: Alert;
    filterAlert: any;
    filterData = null;
    filterList = [];
    fabButtonClicked: boolean = false;
    takeCount = 5;
    hardBackPress: boolean = false;
    hasLoadedImgBool: boolean = false;

    constructor(
        // public appMinimize: AppMinimize,
        public toastCtrl: ToastController,
        public platform: Platform,
        public userProvider: ApiService,
        public loadingCtrl: LoadingController,
        public identityService: IdentityService,
        // public events: Events,
        public navCtrl: NavController,
        private alertCtrl: AlertController,
        private modalCtrl: ModalController,
        private locationService: LocationService,
        private notificationService: NotificationService,
        private cacheService: CacheService) {
        this.initialize();
    }

    initialize() {
        var radius = localStorage.getItem("DistanceRadius");
        if (radius != undefined && radius != null) {
            this.distanceRadius = parseInt(radius);
        }
        this.venueTypes = this.cacheService.getCachedVenueTypes();
        // this.events.subscribe('locationRefreshed', () => {
        //     this.loadSpots(1);
        // });
        this.getVenues(this.pageNumber);
    }

    ionViewDidLoad() {
        // this.refresher.pullMax = 150;
        // this.refresher.pullMin = 150;
        this.getVenues(this.pageNumber);
        setInterval(this.getUserNotifications(), 5000);
    }

    async getVenues(page) {
        this.showLoading = true;

        var spotDistance = new SpotDistanceFilter();
        spotDistance.Page = page;
        spotDistance.Radius = this.distanceRadius;
        spotDistance.Latitude = +localStorage.getItem('Latitude');
        spotDistance.Longitude = +localStorage.getItem('Longitude');
        spotDistance.VenueTypes = this.filterList;
        spotDistance.TakeCount = this.takeCount;

        this.userProvider.getVenues(spotDistance).then((response: any) => {
            this.noInternet = false;
            if (response.WasSuccessful) {
                this.venues = response.Data;
                this.venuesBackup = response.Data;

                if (response.Data.length < this.takeCount) {
                    this.showInfiniteScroll = false;
                    this.showEndOfList = true;
                }
                else {
                    this.showInfiniteScroll = true;
                }

                this.rootTo();
                this.checkHours();
            } else {
                this.loaded = false;
                this.showLoading = false;
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                this.showLoading = false;
                this.loaded = false;

                if (err.status == 0) {
                    this.noInternet = true;
                } else if (err.status == 401) {
                    this.noInternet = false;
                    this.refreshToken();
                } else {
                    this.noInternet = false;
                    this.showErrorMessage(err)
                }

            });
    }

    rootTo() {
        var rootTo = localStorage.getItem("rootTo");
        if (rootTo == "VenueRecommendationReply") {
            // this.openSpot(localStorage.getItem("VenueRecommendationReplyId"), null, null, false);
        }
        else if (rootTo == "linkUp") {
            var tableRequestConfirmation = JSON.parse(localStorage.getItem("tableRequestAccepted"));
            // this.events.publish("navToLinkUp", tableRequestConfirmation);
        }
        this.showLoading = false;
        this.loaded = true;
    }

    getUserNotifications(): any {
        this.userProvider.getUserNotification().then((response: ServiceResult<UserNotificationResult>) => {
            if (response.WasSuccessful) {
                // this.events.publish("updateTabBadge");
            } else {
                // this.events.publish("updateTabBadge");
                console.log(response);
            }
        })
            .catch(err => {
                // this.events.publish("updateTabBadge");
                console.log(err);
            });
    }

    openVenue(venueId, image, tinyImage) {
        var navOptions: NavigationOptions = {
            queryParams: {
                venueId: venueId,
                isModal: false,
                venueImages: {
                    Image: image,
                    TinyImage: tinyImage
                }
            }
        };
        this.navCtrl.navigateForward('/venue', navOptions);
    }

    refreshLocation(refresher) {
        var isGps = this.cacheService.getCachedLocationPreference() == 'GPS';

        if (!isGps) {
            this.doRefresh(refresher);
        }
        else {
            this.locationService.getGpsAvailability().then(response => {
                console.log("gps availability: " + response);
                if (response) {
                    this.locationService.getUserLocation().then(response => {
                        console.log("refresh location success response: " + response);
                        this.doRefresh(refresher);
                    });
                }
                else {
                    this.doRefresh(refresher);
                }
            },
                error => {
                    console.log("refresh location failed response: " + error);
                    this.doRefresh(refresher);
                });
        }
    }

    doRefresh(event) {
        var refresher = event.target;

        this.noInternet = false;
        var spotDistance = new SpotDistanceFilter();
        spotDistance.Page = 1;
        spotDistance.Radius = this.distanceRadius;
        spotDistance.Latitude = +localStorage.getItem('Latitude');
        spotDistance.Longitude = +localStorage.getItem('Longitude');
        spotDistance.VenueTypes = [];
        spotDistance.TakeCount = this.takeCount;

        this.showInfiniteScroll = true;
        this.showEndOfList = false;
        this.pageNumber = spotDistance.Page;
        localStorage.setItem("homepageReload", "1");


        this.userProvider.getVenues(spotDistance).then((response: any) => {
            this.noInternet = false;;
            refresher.complete();
            if (response.WasSuccessful) {
                this.venues = response.Data;
                this.venuesBackup = response.Data;
                this.loaded = true;
                this.checkHours();
            } else {
                this.loaded = false;
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                refresher.complete();
                this.loaded = false;

                if (err.status == 0) {
                    this.noInternet = true;
                    this.pageTop.scrollToTop();
                } else if (err.status == 401) {
                    this.refreshToken();
                } else {
                    this.noInternet = false;
                    this.showErrorMessage(err)
                }

            });
    }

    hasLoadedImg() {
        this.hasLoadedImgBool = true;
    }

    doInfinite(event) {
        var infiniteScroll = event.target;

        var spotDistance = new SpotDistanceFilter();
        this.pageNumber += 1;
        spotDistance.Page = this.pageNumber;
        spotDistance.Radius = this.distanceRadius;
        spotDistance.Latitude = +localStorage.getItem('Latitude');
        spotDistance.Longitude = +localStorage.getItem('Longitude');
        spotDistance.VenueTypes = this.filterList;
        spotDistance.TakeCount = this.takeCount;

        this.userProvider.getVenues(spotDistance).then((response: any) => {
            this.noInternet = false;
            infiniteScroll.complete();
            if (response.WasSuccessful) {
                if (response.Data.length < this.takeCount) {
                    this.showInfiniteScroll = false;
                    this.showEndOfList = true;
                }
                else {
                    this.showInfiniteScroll = true;
                }
                this.venuesBackup.push(...response.Data);

                if (this.pageNumber > 1) {
                    var sorted = this.venuesBackup.sort((a, b) => {
                        return Number(a.Distance) - Number(b.Distance)
                    });
                    this.venuesBackup = sorted;
                }

                this.venues = this.venuesBackup;
                this.checkHours();
            } else {
                if (response.Data.length < 0) {
                    this.showInfiniteScroll = false;
                } else {
                    this.showErrorMessage(response);
                }
                this.showInfiniteScroll = false;
            }
        })
            .catch(err => {
                var errJson = JSON.parse(err._body);
                if (err.status == 0) {
                    setTimeout(() => {
                        this.doInfinite(infiniteScroll);
                    }, 3000);
                }
                else if (errJson.Description == '0 were been found.') {
                    infiniteScroll.complete();
                    this.noInternet = false;
                    this.showInfiniteScroll = false;
                    this.showEndOfList = true;
                }
                else {
                    this.noInternet = false;
                    this.showErrorMessage(err);
                    infiniteScroll.complete();
                }
            });
    }

    showErrorMessage(message) {
        if (message.Heading != undefined) {
            this.notificationService.presentAlertWithDescription(message.Heading, message.Description);
        } else if (message._body != undefined && this.IsJsonString(message._body)) {
            message = JSON.parse(message._body);
            if (message.Description == "Postition Not Found") {
                this.noInternet = false;
            }
            this.notificationService.presentAlertWithDescription(message.Heading, message.Description);
        } else {
            console.log(message.toString());
            this.notificationService.presentAlertWithDescription("An error occured", "An error occured, please ensure you have a internet connection and try again");
        }
    }

    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    checkHours() {
        this.venues.forEach(i => {
            if (i.Hours != null) {
                var hoursAvailable = i.Hours.Days == null ? false : true;
                var timesAdded = false;
                if (hoursAvailable) {
                    i.Hours.Days.forEach(j => {
                        if (j.DayOfWeek == this.dayOfWeek) {
                            i.Times = i.Hours.Days[this.dayOfWeek].Times;
                            timesAdded = true;
                        }
                    });

                    if (!timesAdded) {
                        i.Times = null;
                    }
                }
            }
            else {
                i.Times = null;
            }
        });
    }

    fabClicked() {
        this.fabButtonClicked = !this.fabButtonClicked;
    }

    contentClicked() {
        if (this.fabButtonClicked) {
            // this.fab.close();
            this.fabButtonClicked = false;
        }
    }

    scrollToTop() {
        this.pageTop.scrollToTop();
        // this.fabButtonClicked = !this.fabButtonClicked;
    }

    cancelFilters() {
        if (this.filterList.length != 0) {
            this.filterData = null;
            this.pageNumber = 1;
            this.filterList = [];
            this.venues = [];
            this.loaded = true;
            this.showEndOfList = false;
            this.showInfiniteScroll = false;
            this.pageTop.scrollToTop();
            this.getVenues(1);
        }
        // this.fabButtonClicked = !this.fabButtonClicked;
    }

    async showFilters() {
        this.fabButtonClicked = !this.fabButtonClicked;
        this.filterAlert = await this.alertCtrl.create({
            buttons: [
                {
                    text: 'Cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                        this.fabButtonClicked = !this.fabButtonClicked;
                    }
                },
                {
                    text: 'Ok',
                    handler: data => {
                        console.log('Ok clicked');
                        this.filterData = data;
                        this.venues = [];
                        this.loaded = true;
                        this.pageNumber = 1;
                        this.showInfiniteScroll = false;
                        this.showEndOfList = false;
                        this.showLoading = true;
                        this.filterVenues(data);
                        this.pageTop.scrollToTop();
                        this.fabButtonClicked = !this.fabButtonClicked;
                    }
                }
            ]
        });
        this.filterAlert.header = 'Which type of places would you like to see?';
        // this.filterAlert.setTitle('Which type of places would you like to see?');

        var inputs = [];

        this.venueTypes.forEach(element => {
            // this.filterAlert.addInput({
            //     type: 'checkbox',
            //     label: element.Description,
            //     value: element.Id.toString()
            // });

            inputs.push({
                type: 'checkbox',
                label: element.Description,
                value: element.Id.toString()
            });
        });

        this.filterAlert.inputs = inputs;

        return await this.filterAlert.present();
    }

    filterVenues(selectedVenueTypes) {
        this.filterList = selectedVenueTypes;
        this.getVenues(1);
    }

    async presentSearchVenueModal() {
        this.fabButtonClicked = false;
        
        let searchVenueModal = await this.modalCtrl.create({ component: SearchVenuePage });
        
        searchVenueModal.onDidDismiss().then((() => {
            this.fabClicked();
        }));

        return await searchVenueModal.present();
    }

    refreshToken() {
        var user = this.cacheService.getCachedUser();
        if (user != undefined && user != null) {
            if (user.FacebookId != undefined || user.FacebookId != null) {
                var facebookLoginUserDetails = {
                    EmailAddress: user.EmailAddress,
                    FacebookId: user.FacebookId,
                };
                this.identityService.loginExternalV2(facebookLoginUserDetails)
                    .then((wasSuccessful: boolean) => {
                        if (wasSuccessful) {
                            this.getVenues(1);
                        }
                        else {
                            this.displayAuthenticationFailed();
                        }
                    })
                    .catch(() => {
                        this.displayAuthenticationFailed();
                    });
            }
            else {
                var loginDetails = {
                    Username: user.Username,
                    Password: user.Password,
                }
                this.identityService.authenticateLoginV2(loginDetails)
                    .then((wasSuccessful: boolean) => {
                        if (wasSuccessful) {
                            this.getVenues(1);
                        }
                        else {
                            this.displayAuthenticationFailed();
                        }
                    }).catch(() => {
                        this.displayAuthenticationFailed();
                    });
            }
        }
    }

    displayAuthenticationFailed() {
        // this.events.publish("RouteBroadcastRoot", Constants.SET_ROOT_LANDING_PAGE_MODAL);
    }

    async loadProfile() {
        let profileModal = await this.modalCtrl.create({ component: ProfilePage});
        return await profileModal.present();
    }
}
