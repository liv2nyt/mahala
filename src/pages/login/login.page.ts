import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController, NavParams, ModalController } from '@ionic/angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { IdentityService } from '../../services/identity-service';
// import { Keyboard } from '@ionic-native/keyboard';
import { ForgotPasswordPage } from '../forgot-password/forgot-password.page';
import { UserNameLogin } from '../../contracts/contracts';
import { NotificationService } from '../../services/notification-service';

@Component({
	selector: 'login',
  	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss']
})
export class LoginPage {
	loginForm: FormGroup;
	submitAttempt: boolean = false;
	showPassword: boolean = false;
	isKeyboardHide: boolean = true;
	// @ViewChild(Content) content: Content;
	callback;

	constructor(
		// private keyboard: Keyboard,
		public alertCtrl: AlertController,
		public identityService: IdentityService,
		public navCtrl: NavController,
		public formBuilder: FormBuilder,
		private navParams: NavParams,
		private notificationService: NotificationService,
		private modalCtrl: ModalController
		) {
		this.loginForm = formBuilder.group({
			username: ['', Validators.compose([Validators.minLength(2), Validators.required])],
			password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
		});

		// this.keyboard.onKeyboardShow().subscribe(() => {
		// 	this.isKeyboardHide = false;
		// 	setTimeout(() => {
		// 		this.content.resize();
		// 	}, 100);
		// });

		// this.keyboard.onKeyboardHide().subscribe(() => {
		// 	this.isKeyboardHide = true;
		// 	setTimeout(() => {
		// 		this.content.resize();
		// 	}, 100);
		// });

		this.callback = this.navParams.get("callback");
	}

	// cancel() {
	// 	this.viewCtrl._destroy();
	// }

	// closeKeyboard() {
	// 	this.keyboard.hide();
	// }

	loginUser() {
		this.submitAttempt = true;
		if (!this.loginForm.controls.password.valid || !this.loginForm.controls.username.valid) {
			const alert = this.alertCtrl.create({
				header: 'Incomplete Form',
				// title: 'Incomplete Form',
				message: 'Please complete all the fields correctly and then try again',
				// subTitle: 'Please complete all the fields correctly and then try again',
				buttons: ['OK']
			});
			// alert.present();
			
		} else {
			var loginDetails: UserNameLogin = {
				Username: this.loginForm.value.username,
				Password: this.loginForm.value.password,
			}
			this.identityService.authenticateLoginV2(loginDetails)
				.then(() => {
					this.modalCtrl.dismiss(true);
					// this.viewCtrl._destroy();
				})
				.catch(() => {
					this.notificationService.presentAlert('Login Failed, please try again.');
				});
		}
	}

	// forgotPassword() {
	// 	this.navCtrl.navigateForward('/forgot-password');
	// }

	async forgotPassword() {
		let forgotPasswordPageModal = await this.modalCtrl.create({
		  component: ForgotPasswordPage
		});
		return await forgotPasswordPageModal.present();
	  }

	  goBack()
	  {
		  this.modalCtrl.dismiss();
	  }
}
