import { Component } from "@angular/core";
import { AlertController, ModalController } from "@ionic/angular";
import {
    FormBuilder,
    FormGroup,
    Validators,
    FormControl
} from "@angular/forms";
import { IdentityService } from "../../services/identity-service";
import { EmailValidator } from "../../validators/email";
import { ImageService } from "../../services/image-service";
import { Headers } from "@angular/http";
// import { Keyboard } from "@ionic-native/keyboard";
import { ApiService } from "../../services/api-service";
import { CacheService } from "../../services/cache-service";
import { NotificationService } from "../../services/notification-service";
import { ImageResult } from 'src/contracts/contracts';

@Component({
    selector: "registration",
    templateUrl: "./registration.page.html",
    styleUrls: ["./registration.page.scss"]
})
export class RegistrationPage {
    registrationForm: FormGroup;
    submitAttempt: boolean = false;
    step: string = "1";
    countryCode: string = "+27";
    showPassword: boolean = false;
    imageUploadedSuccess: boolean = false;
    imageUrl: any = "assets/imgs/profile-imgs/user.svg";
    tinyImageUrl: any = "assets/imgs/profile-imgs/user.svg";

    constructor(
        private notificationService: NotificationService,
        // private keyboard: Keyboard,
        private imageService: ImageService,
        private alertCtrl: AlertController,
        private identityService: IdentityService,
        private formBuilder: FormBuilder,
        private userProvider: ApiService,
        private modalCtrl: ModalController,
        private cacheService: CacheService
    ) {
        this.initialize();
    }

    initialize() {
        this.createForm();
    }

    createForm() {
        let checkUsername = (control: FormControl): any => {
            var headers = new Headers();
            headers.append("Content-Type", "application/json; charset=utf-8");

            return new Promise(resolve => {
                var usernameToCheck = control.value.toLowerCase();
                this.userProvider
                    .IsUsernameAvailable(usernameToCheck)
                    .then(() => {
                        resolve(null);
                    })
                    .catch(() => {
                        resolve({
                            "username taken": true
                        });
                    });
            });
        };
        this.registrationForm = this.formBuilder.group({
            username: [
                "",
                Validators.compose([
                    Validators.minLength(2),
                    Validators.required
                ]),
                checkUsername
            ],
            FirstName: [
                "",
                Validators.compose([
                    Validators.required,
                    Validators.pattern("[a-zA-Z ]*")
                ])
            ],
            LastName: [
                "",
                Validators.compose([
                    Validators.required,
                    Validators.pattern("[a-zA-Z ]*")
                ])
            ],
            gender: ["Female"],
            hashedPassword: [
                "",
                Validators.compose([
                    Validators.minLength(6),
                    Validators.required
                ])
            ],
            emailAddress: ["", EmailValidator.isEmailValid],
            CellNumber: [
                "",
                Validators.compose([
                    Validators.minLength(9),
                    Validators.required
                ])
            ],
            image: [""]
        });
    }

    // closeKeyboard() {
    //     this.keyboard.hide();
    // }

    async goToNextStep() {
        this.submitAttempt = true;
        switch (this.step) {
            case "1":
                if (
                    !this.registrationForm.controls.hashedPassword.valid ||
                    !this.registrationForm.controls.username.valid ||
                    !this.registrationForm.controls.FirstName.valid ||
                    !this.registrationForm.controls.LastName.valid
                ) {
                    const alert = await this.alertCtrl.create({
                        header: "Incomplete Form",
                        message:
                            "Please complete all the fields correctly and then try again",
                        buttons: ["OK"]
                    });
                    return await alert.present();
                } else {
                    if (!this.registrationForm.controls.emailAddress.valid) {
                        const alert = await this.alertCtrl.create({
                            header: "Invalid Email Address",
                            message:
                                "Please ensure you have correctly entered your email address and try again",
                            buttons: ["OK"]
                        });
                        return await alert.present();
                    }
                    else if(!this.registrationForm.value.emailAddress) {
                        const alert = await this.alertCtrl.create({
                            header: "Email Address is Required",
                            message:
                                "Please enter a email address as it is required for registration",
                            buttons: ["OK"]
                        });
                        return await alert.present();
                    }
                    else{
                        this.step = "2";
                    }
                }
                break;
            case "2":
                    if (!this.registrationForm.controls.CellNumber.valid || this.countryCode == "") 
                    {
                        const alert = await this.alertCtrl.create({
                            header: "Invalid Mobile Number",
                            message:
                                "Please ensure you have correctly entered your mobile number and country code and try again",
                            buttons: ["OK"]
                        });
                        return await alert.present();
                    } else {
                        if(this.registrationForm.value.CellNumber)
                        {
                            if (this.countryCode.substring(0, 1) != "+") {
                                var countryCodeStr = "+" + this.countryCode;
                                this.countryCode = countryCodeStr;
                            }
    
                            if (this.registrationForm.value.CellNumber.substring(0,1) == "0") 
                            {
                                var mobileNumberStr = this.registrationForm.value.CellNumber.substring(
                                    1,
                                    this.registrationForm.value.CellNumber.length
                                );
                                this.registrationForm.value.CellNumber = mobileNumberStr;
                            }
                            this.registerUser();
                        }
                    }
                break;
            case "3":
                this.goBack(true);
                break;
        }
    }

    goBack(data) {
        this.modalCtrl.dismiss(data);
    }

    uploadImage() {
        this.imageService
            .uploadImage(this.userProvider.url + "/Image/User", "Profile")
            .then((response: ImageResult) => {
                var user = this.cacheService.getCachedUser();
                user.Image = response.Image;
                user.TinyImage = response.TinyImage;
                user.MediumImage = response.MediumImage;
                this.cacheService.setCachedUser(user);
                this.goBack(true);
            })
            .catch(error => {
                console.log(error);
                this.notificationService.presentAlertWithDescription(
                    "Failed to Upload Profile Image",
                    "The image failed to upload, please ensure you have an internet connection and try again"
                );
            });
    }

    cancel() {
        switch (this.step) {
            case "1":
                this.goBack(false);
                break;
            case "2":
                this.step = "1";
                break;
        }
    }

    skipStep() {
        switch (this.step) {
            case "2":
                this.registerUser();
                break;
            case "3":
                this.goBack(true);
                break;
        }
    }

    registerUser() {
        if (this.registrationForm.value.CellNumber && this.registrationForm.value.CellNumber.substring(0, 1) != "+") {
            this.registrationForm.value.CellNumber =
                this.countryCode + this.registrationForm.value.CellNumber;
        }
        else {
            this.registrationForm.value.CellNumber = null;
        }

        this.registrationForm.value.username.trim();
        this.registrationForm.value.hashedPassword.trim();
        this.identityService
            .registerUserV2(this.registrationForm.value)
            .then(() => {
                this.step = "3";
            })
            .catch(() => {
                console.log("Registration Failed");
            });
    }
}
