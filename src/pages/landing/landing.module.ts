import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { LandingPage } from './landing.page';

const routes: Routes = [
  {
    path: "",
    component: LandingPage
  }
];

@NgModule({
  imports: [
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LandingPage]
})
export class LandingPageModule {}
