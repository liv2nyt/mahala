import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ToastController, ModalController, Platform } from '@ionic/angular';
import { RegistrationPage } from '../registration/registration.page';
import { IdentityService } from '../../services/identity-service'
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { FacebookRegistrationPage } from '../facebook-registration/facebook-registration.page';
import { CacheService } from '../../services/cache-service';
import { LoginResult } from '../../contracts/contracts';
import { NotificationService } from '../../services/notification-service';
import { Constants } from '../../contracts/constants';
import { LoginPage } from '../login/login.page';
import { Components } from '@ionic/core';
import { PointsEarnedPage } from '../trophy-room/points-earned/points-earned.page';
import { NewLevelPage } from '../trophy-room/new-level/new-level.page';
import { DirectMessagePage } from '../trophy-room/direct-message/direct-message.page';
import { BadgeEarnedPage } from '../trophy-room/badge-earned/badge-earned.page';

@Component({
  selector: 'landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss']
})
export class LandingPage implements OnInit {
  hardBackPress: boolean = false;
  @Input() modal: Components.IonModal;
  
  constructor(
    public toastCtrl: ToastController,
    private fb: Facebook,
    public identityService: IdentityService,
    public platform: Platform,
    public alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private cacheService: CacheService,
    private notificationService: NotificationService) {
  }

  ngOnInit()
  {
    this.identityService.initializeUser();
  }

  goBack(message: string) {
    switch (message) {
      case Constants.LOGIN_SUCCESSFUL:
          this.identityService.validateLocationSettings();
          break;
      case Constants.REGISTRATION_SUCCESSFUL:
          this.identityService.openWalkthroughPage();
          break;
      case Constants.FACEBOOK_LOGIN_SUCCESSFUL:
          this.identityService.validateLocationSettings();
          break;
      case Constants.FACEBOOK_REGISTRATION_SUCCESSFUL:
          this.identityService.openWalkthroughPage();
          break;
      default:
          break;
    }
    // this.modal.dismiss(message);
  }

  async login() {
    let loginPageModal = await this.modalCtrl.create({
      component: LoginPage
    });
    loginPageModal.onDidDismiss().then((response) => {
      if (response.data) {
        this.goBack(Constants.LOGIN_SUCCESSFUL);
      }
    });
    return await loginPageModal.present();
  }

  async register() {
    let registerModal = await this.modalCtrl.create({
      component: RegistrationPage
    });
    registerModal.onDidDismiss().then((response) => {
      if (response.data) {
        this.goBack(Constants.REGISTRATION_SUCCESSFUL);
      }
    });
    return await registerModal.present();
  }

  facebookLogin() {
    this.fb.login(['public_profile', 'email'])
      .then((res: FacebookLoginResponse) => {
        console.log('Logged into Facebook!', res);
        var user = this.cacheService.getCachedUser();
        if (user == null || user == undefined) {
          user = new LoginResult();
        }
        user.FacebookAuthToken = res.authResponse.accessToken;
        this.cacheService.setCachedUser(user);
        // localStorage.setItem('FacebookAuthToken', res.authResponse.accessToken);
        if (res.status == 'connected') {
          this.getUserFacebookDetailsV2();
        }
      })
      .catch(e => {
        this.notificationService.presentAlertWithDescription('Error logging into Facebook', "Please ensure you have the facebook app installed on your device and try again");
        console.log('Error logging into Facebook', e.toString());
      });
  }

  getUserFacebookDetailsV2() {
    this.fb.api('/me?fields=id,name,email,picture.type(large),permissions', null).then((response: any) => {
      var user = this.cacheService.getCachedUser();
      if (user == null || user == undefined) {
        user = new LoginResult();
      }
      var firstName = response.name.split(' ').slice(0, -1).join(' ');
      var lastName = response.name.split(' ').slice(-1).join(' ');

      user.Image = response.picture.data.url;
      user.FirstName = firstName;
      user.LastName = lastName;
      user.FacebookId = response.id;
      user.EmailAddress = response.email;
      this.cacheService.setCachedUser(user);

      var facebookLoginUserDetails = {
        EmailAddress: response.email,
        FacebookId: response.id,
      };
      this.identityService.loginExternalV2(facebookLoginUserDetails)
        .then((firstTimeRegistration) => {
          if (firstTimeRegistration) {
            this.openFacebookRegistrationModal();
          }
          else {
            this.goBack(Constants.FACEBOOK_LOGIN_SUCCESSFUL);
          }
        });
    })
      .catch(e => {
        this.notificationService.presentAlertWithDescription('Error retrieving information from facebook', "Please try again and ensure you have provided access to Mahala");
        console.log('Error retrieving user information from facebook', e.toString());
      });
  }

  async openFacebookRegistrationModal() {
    let facebookRegistrationModal = await this.modalCtrl.create({
      component: FacebookRegistrationPage
    });
    facebookRegistrationModal.onDidDismiss().then((response) => {
      if (response.data) {
        this.goBack(Constants.FACEBOOK_REGISTRATION_SUCCESSFUL);
      }
    });
    return await facebookRegistrationModal.present();
  }
}
