import { Component } from "@angular/core";
import { ModalController, NavParams } from "@ionic/angular";

@Component({
    selector: 'reward-claimed',
    templateUrl: './reward-claimed.page.html',
    styleUrls: ['./reward-claimed.page.scss']
})
export class RewardClaimedPage {
    points: number;

    constructor(
        private navParams: NavParams,
        private modalCtrl: ModalController) {
    }

    // ionViewDidEnter() {
    //     // this.points = this.navParams.get('points');
    // }

    close() {
        this.modalCtrl.dismiss(false);
    }
}