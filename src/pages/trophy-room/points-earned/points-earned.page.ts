import { Component } from "@angular/core";
import { ModalController, NavParams } from "@ionic/angular";

@Component({
    selector: 'points-earned',
    templateUrl: './points-earned.page.html',
    styleUrls: ['./points-earned.page.scss']
})
export class PointsEarnedPage {
    points: number;

    constructor(
        private navParams: NavParams,
        private modalCtrl: ModalController) {
    }

    // ionViewDidEnter() {
    //     // this.points = this.navParams.get('points');
    // }

    close() {
        this.modalCtrl.dismiss(false);
    }
}