import { Component } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { NewUserBadgePushResult, UserBadgeResult } from '../../../contracts/contracts';

@Component({
  selector: 'direct-message',
  templateUrl: './direct-message.page.html',
  styleUrls: ['./direct-message.page.scss']
})
export class DirectMessagePage {
  venueName: string;
  message: string;

  constructor(
    private modalCtrl: ModalController,
    private navParams: NavParams) {
  }

  ionViewDidEnter() {
    // this.venueName = this.navParams.get('venueName');
    // this.message = this.navParams.get('message');
  }

  close() {
    this.modalCtrl.dismiss();
  }
}
