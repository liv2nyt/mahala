import { Component } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { CacheService } from '../../../services/cache-service';
import { LoginResult, Level } from '../../../contracts/contracts';

@Component({
  selector: 'new-level',
  templateUrl: './new-level.page.html',
  styleUrls: ['./new-level.page.scss']
})
export class NewLevelPage {
  level: Level;
  user: LoginResult;
  levelId: number;

  constructor(
    private modalCtrl: ModalController,
    private navParams: NavParams,
    private cacheService: CacheService) {
  }

  ionViewDidEnter() {
    // this.levelId = this.navParams.get('levelId');
    var levels = this.cacheService.getCachedLevels();
    this.level = levels.filter(c => c.Id == this.levelId)[0];
    this.user = this.cacheService.getCachedUser();
  }

  close() {
    this.modalCtrl.dismiss();
  }
}
