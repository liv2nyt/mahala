import { Component } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { NewUserBadgePushResult, UserBadgeResult } from 'src/contracts/contracts';

@Component({
  selector: 'badge-earned',
  templateUrl: './badge-earned.page.html',
  styleUrls: ['./badge-earned.page.scss']
})
export class BadgeEarnedPage {
  newUserBadgePushResult: NewUserBadgePushResult;
  badge: UserBadgeResult;
  venueName: string;
  friendFullName: string;

  constructor(
    private modalCtrl: ModalController,
    private navParams: NavParams) {
  }

  ionViewDidEnter() {
    // this.newUserBadgePushResult = this.navParams.get('newUserBadgePushResult');
    // this.friendFullName = this.navParams.get('friendFullName');
    this.badge = this.newUserBadgePushResult.NewUserBadgeResult;
    this.venueName = this.newUserBadgePushResult.Venue.Name;
  }

  close() {
    this.modalCtrl.dismiss();
  }
}
