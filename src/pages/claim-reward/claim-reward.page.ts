import { Component } from "@angular/core";
import { AlertController, ModalController } from "@ionic/angular";
import { UserReward } from 'src/contracts/contracts';
import { EventService } from 'src/services/event-service';

@Component({
    templateUrl: './claim-reward.page.html',
    selector: 'claim-reward',
    styleUrls: ['./claim-reward.page.scss']
})
export class ClaimRewardPage {
    // userReward: UserReward;
    userReward: any;
    rewardCode = "";

    constructor(
        private modalCtrl: ModalController,
        private alertCtrl: AlertController,
        private eventService: EventService) {
    }

    ionViewWillEnter() {
        this.userReward = this.userReward;
        this.rewardCode = this.userReward.QrCode;
        this.initRewardClaim();
    }

    initRewardClaim() {
        this.eventService.rewardClaimedSource$.subscribe(value => {
            this.goBack();
        });
    }

    // async displayRewarmClaimedAlert()
    // {
    //     var alert = await this.alertCtrl.create({
    //         header: "Reward Claimed",
    //         message: "Your reward was claimed successfully",
    //         buttons: [{
    //             text: "Ok",
    //             handler: () => {
    //                 this.goBack();
    //             }
    //         }]
    //     });

    //     return await alert.present();
    // }

    goBack() {
        this.modalCtrl.dismiss();
    }
}