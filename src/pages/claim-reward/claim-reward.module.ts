import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angular2-qrcode';
import { ClaimRewardPage } from './claim-reward.page';

const routes: Routes = [
  {
    path: "",
    component: ClaimRewardPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    QRCodeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ClaimRewardPage]
})
export class QrCodePageModule {}


