import { Component, OnInit } from '@angular/core';
import { NavParams, LoadingController, Platform, AlertController, ModalController, PopoverController, NavController } from '@ionic/angular';
import { ApiService } from 'src/services/api-service';
import { NotificationService } from 'src/services/notification-service';
import { VenuePage } from '../venue/venue.page';
import { CacheService } from 'src/services/cache-service';
import { Router } from '@angular/router';
import { LandingPage } from '../landing/landing.page';
import { LocationPage } from '../location/location.page';
// import { PhotoViewer } from '@ionic-native/photo-viewer';

@Component({
    selector: 'profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss']
})
export class ProfilePage implements OnInit {
    firstName: string;
    lastName: string;
    username: string;
    gender: string;
    mobileNum: string;
    email: string;
    Image: string = "assets/imgs/profile-imgs/user.svg";
    TinyImage: string = "assets/imgs/profile-imgs/user.svg";
    MediumImage: string = "assets/imgs/profile-imgs/user.svg";
    userId: any;
    isFriend: boolean = false;
    friendReqSent: boolean = true;
    recommendedVenues = [];
    venuesVisited = [];
    invitations = [];
    loading: boolean = false;
    popover;
    noInternet:boolean = false;
    
    constructor(
        public navParams: NavParams,
        public userProvider: ApiService,
        public loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        public modalCtrl: ModalController,
        public platform: Platform,
        // private photoViewer: PhotoViewer,
        private popoverCtrl: PopoverController,
        private notificationService: NotificationService,
        private cacheService: CacheService,
        private router: Router
        ) {
        this.userId = this.navParams.get("userId");
        // this.popover = this.popoverCtrl.create("ProfilePopoverPage");
    }

    ngOnInit() {
        this.getProfile();
    }

    // presentPopover(myEvent, firstName, lastName) {
    //     this.popover.onWillDismiss(data => {
    //         if (data != null) {
    //             if (data.RemoveFriend) {
    //                 this.removeFriendAlert(firstName, lastName);
    //             }
    //         }
    //     })
    //     this.popover.present({
    //         ev: myEvent
    //     });
    // }

    // openImage() {
    //     this.photoViewer.show(this.Image + "?" + new Date().getTime(), this.firstName + ' ' + this.lastName, Constants.PHOTO_VIEWER_OPTIONS);
    // }

    getProfile()
    {
        this.loading = true;
        var user = this.cacheService.getCachedUser();

        this.firstName = user.FirstName;
        this.lastName = user.LastName;
        this.username = user.Username;
        // this.mobileNum = user.CellNumber;
        this.gender = user.Gender;
        this.email = user.EmailAddress;
        this.Image = user.Image;
        this.TinyImage = user.TinyImage;
        this.MediumImage = user.MediumImage;

        if (this.Image == null || this.Image == undefined || this.Image == "") {
            this.Image = "assets/imgs/profile-imgs/user.svg";
            this.TinyImage = "assets/imgs/profile-imgs/user.svg";
        }
        else {
            this.Image = user.Image + "?" + new Date().getTime();
            this.TinyImage = user.TinyImage + "?" + new Date().getTime();
        }
        if (this.MediumImage == null || this.MediumImage == undefined || this.MediumImage == "") {
            this.MediumImage = "assets/imgs/profile-imgs/user.svg";
        }
        else {
            this.MediumImage = user.MediumImage + "?" + new Date().getTime();
        }
        this.loading = false;
    }

    goBack() {
        // this.popover.dismiss();
        var data = {
            RemoveFriend: false,
            AddFriend: false
        };
        this.modalCtrl.dismiss(data);
    }

    async logoutAlert() {
        const confirm = await this.alertCtrl.create({
            message: 'Are sure you would like to logout?',
            buttons: [
                {
                    text: 'No'
                },
                {
                    text: 'Yes',
                    handler: () => {
                        this.logout();
                    }
                }
            ]
        });
        
        return await confirm.present();
    }

    logout() {
        var VenueNotificationsInfoCount = localStorage.getItem("VenueNotificationsInfoCount");
        var venueTypes = this.cacheService.getCachedVenueTypes();
        localStorage.clear();
        localStorage.setItem("VenueNotificationsInfoCount", VenueNotificationsInfoCount);
        localStorage.setItem('hasLoggedIn', '0');
        this.cacheService.setCachedVenueTypes(venueTypes);
        // this.getVersionNumber();
        // this.events.publish('RouteBroadcastRoot', Constants.SET_ROOT_LANDING_PAGE_MODAL);
        
        // this.navCtrl.navigateRoot('');
        this.router.navigate(['/landing']);

        this.goBack();
    }

    // async getVersionNumber() {
    //     const versionNumber = await this.appVersion.getVersionNumber();
    //     localStorage.setItem('version', versionNumber);
    //     console.log(versionNumber);
    // }

    async loadLocationSettings() {
        var locationModal = await this.modalCtrl.create({ 
            component: LocationPage, 
            componentProps: { openAsProvider: false }
        });
        locationModal.present();
    }
}
